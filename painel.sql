-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Tempo de geração: 29/03/2022 às 13:30
-- Versão do servidor: 10.4.22-MariaDB
-- Versão do PHP: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `painel`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_cardcredituser`
--

CREATE TABLE `tb_cardcredituser` (
  `id_cardcredit` int(11) NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `desnameholder` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deslastnumber` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desbrand` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desdateexpiration` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_cardcredituser`
--

INSERT INTO `tb_cardcredituser` (`id_cardcredit`, `idtransporter`, `desnameholder`, `deslastnumber`, `desbrand`, `desdateexpiration`, `dtregister`) VALUES
(10, 52, 'Denilson Dias', '8598', 'mastercard', '1029', '2022-03-01 14:57:34');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_drivers`
--

CREATE TABLE `tb_drivers` (
  `iddriver` int(11) NOT NULL,
  `idtransporter` int(11) DEFAULT NULL,
  `token_driver` varchar(11) DEFAULT NULL,
  `confirm_register` tinyint(4) NOT NULL DEFAULT 0,
  `confirm_identity` tinyint(4) NOT NULL DEFAULT 0,
  `name_driver` varchar(64) DEFAULT NULL,
  `cpf_driver` varchar(128) DEFAULT NULL,
  `cep_driver` varchar(128) DEFAULT NULL,
  `city_driver` varchar(128) DEFAULT NULL,
  `neighborhood_driver` varchar(128) DEFAULT NULL,
  `street_driver` varchar(126) DEFAULT NULL,
  `birthdate_driver` varchar(128) DEFAULT NULL,
  `nrphone_driver` varchar(125) NOT NULL,
  `email_driver` varchar(125) NOT NULL,
  `pis_driver` varchar(11) NOT NULL DEFAULT '0',
  `confirmregister` tinyint(1) NOT NULL DEFAULT 0,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `tb_drivers`
--

INSERT INTO `tb_drivers` (`iddriver`, `idtransporter`, `token_driver`, `confirm_register`, `confirm_identity`, `name_driver`, `cpf_driver`, `cep_driver`, `city_driver`, `neighborhood_driver`, `street_driver`, `birthdate_driver`, `nrphone_driver`, `email_driver`, `pis_driver`, `confirmregister`, `dtregister`) VALUES
(282, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(283, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(284, 53, '580146', 0, 0, 'Vitor Santos\r\n', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(285, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(286, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(287, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(288, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(289, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(290, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(291, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(292, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(293, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(294, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(295, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(296, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(297, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(298, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(299, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(300, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38'),
(301, 52, '3362246', 0, 0, 'Denilson Dias', '43060973822', '03875000', 'São Paulo', 'Vila Rio Branco', 'Rua Sancho Junqueira', '15062000', '11986569023', 'denilsondias277@gmail.com', '65206792349', 1, '2022-03-05 00:44:46'),
(302, 52, '580146', 0, 0, 'Rodrigo Fernandes Malagutti', '27256384882', '13212300', 'Jundiaí', 'Chácara Segre', 'Rua Sérgio Gregio, 50', '22041978', '11963024023', 'ro_malagutti@hotmail.com', '0', 0, '2022-03-11 13:13:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_drivers_general`
--

CREATE TABLE `tb_drivers_general` (
  `iddriver` int(11) NOT NULL,
  `desname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descpf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nrphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_drivers_general`
--

INSERT INTO `tb_drivers_general` (`iddriver`, `desname`, `descpf`, `nrphone`, `dtregister`) VALUES
(2, 'Denilson Dias', '43060973822', '11986569023', '2021-11-01 23:27:51'),
(3, 'Denilson Dias', '43060973822', '11986569023', '2021-11-01 23:27:51'),
(5, 'José Santos', '29476557885', '11956573257', '2021-11-02 12:04:28');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_folder`
--

CREATE TABLE `tb_folder` (
  `idfolder` int(11) NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `iddriver` int(11) NOT NULL,
  `namefolder` varchar(256) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_folder`
--

INSERT INTO `tb_folder` (`idfolder`, `idtransporter`, `iddriver`, `namefolder`) VALUES
(8, 6, 35, 'Nome aleatório');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_folderdrivers`
--

CREATE TABLE `tb_folderdrivers` (
  `idfolderdrivers` int(11) NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `iddriver` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_folders_backup`
--

CREATE TABLE `tb_folders_backup` (
  `idfolder` int(11) NOT NULL,
  `desname` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `desdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_folders_backup`
--

INSERT INTO `tb_folders_backup` (`idfolder`, `desname`, `idtransporter`, `desdate`) VALUES
(55, '00000001244', 52, '2022-03-02'),
(57, '40252971833', 52, '2022-03-02'),
(65, '04046583851', 52, '2022-03-04'),
(67, '27256384882', 52, '2022-03-04'),
(69, '43060973822', 52, '2022-03-04'),
(70, '62819100953', 52, '2022-03-05'),
(71, '00000065480', 52, '2022-03-05'),
(72, '00000001163', 52, '2022-03-05'),
(73, '00000059323', 52, '2022-03-05'),
(74, '00000071889', 52, '2022-03-05'),
(75, '00000086720', 52, '2022-03-05'),
(76, '11456458876', 52, '2022-03-05'),
(77, '22222222222', 52, '2022-03-16');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_notifications_services`
--

CREATE TABLE `tb_notifications_services` (
  `idnotification` int(11) NOT NULL,
  `notification` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'A transportadora "nome" lhe enviou uma solicitação de serviço',
  `messageDriver` text COLLATE utf8_unicode_ci NOT NULL,
  `iddriver` int(11) NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `request` tinyint(4) DEFAULT 0,
  `updateDate` datetime NOT NULL,
  `stateRead` tinyint(4) NOT NULL DEFAULT 0,
  `serviceaccept` tinyint(4) NOT NULL DEFAULT 0,
  `servicerejected` tinyint(1) NOT NULL DEFAULT 0,
  `attempts` tinyint(4) NOT NULL,
  `attempst_time` time DEFAULT NULL,
  `tokenconfirmeddriver` tinyint(1) NOT NULL DEFAULT 0,
  `driverreadmessage` tinyint(4) NOT NULL DEFAULT 0,
  `dt_register` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_notifications_services`
--

INSERT INTO `tb_notifications_services` (`idnotification`, `notification`, `message`, `messageDriver`, `iddriver`, `idtransporter`, `status`, `request`, `updateDate`, `stateRead`, `serviceaccept`, `servicerejected`, `attempts`, `attempst_time`, `tokenconfirmeddriver`, `driverreadmessage`, `dt_register`) VALUES
(54, '2210025', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 10:04:49', 2, 1, 0, 0, NULL, 1, 1, '2022-03-04 21:48:58'),
(55, '1031188', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 10:34:36', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 10:28:35'),
(56, '3047866', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'O motorista  rejeitou a sua solicitação de serviço', 282, 52, 3, 2, '2022-03-06 10:48:47', 2, 0, 1, 0, NULL, 0, 1, '2022-03-06 10:45:30'),
(57, '3625071', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 10:52:31', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 10:50:09'),
(58, '2747587', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 10:53:50', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 10:52:41'),
(59, '952871', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 10:58:13', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 10:54:01'),
(60, '3765893', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 11:06:24', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 10:58:31'),
(61, '353488', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'O motorista  rejeitou a sua solicitação de serviço', 282, 52, 3, 2, '2022-03-06 11:38:24', 2, 0, 1, 0, NULL, 0, 1, '2022-03-06 11:06:33'),
(62, '970979', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 11:39:40', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 11:38:35'),
(63, '1958279', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-06 12:04:20', 2, 1, 0, 0, NULL, 1, 1, '2022-03-06 11:39:57'),
(64, '3908262', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'O motorista  rejeitou a sua solicitação de serviço', 282, 52, 3, 2, '2022-03-07 16:49:35', 2, 0, 1, 0, NULL, 0, 1, '2022-03-07 16:15:15'),
(65, '383247', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 2, '2022-03-07 17:03:40', 2, 0, 1, 0, NULL, 1, 1, '2022-03-07 16:58:46'),
(66, '718556', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', 'Denilson Dias acabou de confirmar seu token de validação.', 282, 52, 2, 1, '2022-03-07 17:44:56', 2, 1, 0, 0, NULL, 1, 1, '2022-03-07 17:02:45'),
(67, '1175886', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', '', 282, 52, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, NULL, 0, 1, '2022-03-14 10:32:18'),
(68, '3878654', 'A transportadora Transportadora Rio Branco lhe enviou uma solicitação de serviço.', '', 282, 52, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, NULL, 0, 0, '2022-03-16 19:47:18');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_persons`
--

CREATE TABLE `tb_persons` (
  `idperson` int(11) NOT NULL,
  `desperson` varchar(64) NOT NULL,
  `desemail` varchar(128) DEFAULT NULL,
  `nrphone` bigint(20) DEFAULT NULL,
  `desimg` varchar(255) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `tb_persons`
--

INSERT INTO `tb_persons` (`idperson`, `desperson`, `desemail`, `nrphone`, `desimg`, `dtregister`) VALUES
(50, 'Denilson Dias', 'denilsondias59@gmail.com', 11986569023, '/views/assets/img/admins/6161d94b0fabb.jpg', '2021-09-10 16:08:43'),
(98, 'Token Validador', 'tokenvalidador@loggitech.log.br', 1145252895, '/views/assets/img/admins/61d5a0f8307bb.jpg', '2021-10-15 23:56:55');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_plans`
--

CREATE TABLE `tb_plans` (
  `idplan` int(11) NOT NULL,
  `desnumber` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `desplan` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `desbenefits` text COLLATE utf8_unicode_ci NOT NULL,
  `desprice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plans_benefits` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `limitsearch` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_plans`
--

INSERT INTO `tb_plans` (`idplan`, `desnumber`, `desplan`, `desbenefits`, `desprice`, `plans_benefits`, `status`, `limitsearch`, `dtregister`) VALUES
(47, '1970917', 'Plano 01 - Teste', 'Beneficio 01', '500', '100', 0, '500', '2022-03-01 14:57:20');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_planuser`
--

CREATE TABLE `tb_planuser` (
  `idplanuser` int(11) NOT NULL,
  `idtransporter` int(11) DEFAULT NULL,
  `desnameplan` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desbenefit` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_update` date NOT NULL,
  `first_update` int(11) NOT NULL DEFAULT 0,
  `dt_register` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_planuser`
--

INSERT INTO `tb_planuser` (`idplanuser`, `idtransporter`, `desnameplan`, `desbenefit`, `date_update`, `first_update`, `dt_register`) VALUES
(11, 40, '0', '0', '0000-00-00', 0, '2022-01-04 08:21:24'),
(12, 41, 'Plano 5 teste', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s<br> when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries<br> but also the leap into electronic typesetting<br> remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages<br> and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '0000-00-00', 0, '2022-01-04 08:24:15'),
(13, 6, 'Plano Padrão', 'Token Validador (Limite de 2.000 disparos)<br>  Suporte Técnico e Help Desk<br> Armazenamento e Backup dos dados<br> Importação de dados e integrações<br> Validade para utilização 12 meses', '2021-01-02', 1, '2022-01-04 08:34:20'),
(14, 43, 'name plan', '500 pesquisas', '0000-00-00', 0, '2022-01-05 08:18:37'),
(15, 44, '', '', '0000-00-00', 0, '2022-01-05 08:29:29'),
(16, 45, '', '', '0000-00-00', 0, '2022-01-05 08:31:34'),
(17, 46, '', '', '0000-00-00', 0, '2022-01-05 08:43:10'),
(18, 47, '', '', '0000-00-00', 0, '2022-01-05 08:43:20'),
(19, 48, '', '', '0000-00-00', 0, '2022-01-05 08:57:52'),
(20, 49, '', '', '0000-00-00', 0, '2022-01-05 09:03:37'),
(21, 50, 'Plano 5 teste', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s<br> when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries<br> but also the leap into electronic typesetting<br> remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages<br> and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '0000-00-00', 0, '2022-01-05 09:06:58'),
(22, 51, 'Plano Padrão', 'Token Validador (Limite de 2.000 disparos)<br>  Suporte Técnico e Help Desk<br> Armazenamento e Backup dos dados<br> Importação de dados e integrações<br> Validade para utilização 12 meses', '0000-00-00', 0, '2022-01-06 16:36:53'),
(23, 52, 'Plano 01 - Teste', 'Beneficio 01', '0000-00-00', 0, '2022-03-01 11:57:34');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_reset_tokens`
--

CREATE TABLE `tb_reset_tokens` (
  `idreset_tokens` int(11) NOT NULL,
  `idtransporter` int(11) NOT NULL,
  `date_update` date NOT NULL,
  `first_update` int(11) NOT NULL DEFAULT 0,
  `last_update` datetime NOT NULL,
  `dt_register` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_reset_tokens`
--

INSERT INTO `tb_reset_tokens` (`idreset_tokens`, `idtransporter`, `date_update`, `first_update`, `last_update`, `dt_register`) VALUES
(39, 52, '2022-03-01', 0, '0000-00-00 00:00:00', '2022-03-01 11:57:34');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_transporters`
--

CREATE TABLE `tb_transporters` (
  `idtransporter` int(11) NOT NULL,
  `idpagarme` int(11) NOT NULL,
  `idplan` int(11) NOT NULL,
  `tokenssearchused` varchar(8) NOT NULL DEFAULT '0',
  `countmytokens` varchar(8) NOT NULL DEFAULT '0',
  `destransporter` varchar(64) DEFAULT NULL,
  `descnpj` varchar(128) DEFAULT NULL,
  `descep` varchar(128) DEFAULT NULL,
  `descity` varchar(128) DEFAULT NULL,
  `desstreet` varchar(126) DEFAULT NULL,
  `desneighborhood` varchar(128) DEFAULT NULL,
  `desstreetnumber` varchar(128) DEFAULT NULL,
  `nrphonetransporter` varchar(125) NOT NULL,
  `desemailtransporter` varchar(125) DEFAULT NULL,
  `desrole` varchar(128) DEFAULT NULL,
  `desimg` text NOT NULL,
  `deschangebenefit` varchar(255) DEFAULT NULL,
  `deslimitsearch` varchar(64) NOT NULL,
  `state_read` tinyint(4) NOT NULL DEFAULT 0,
  `firstaccess` tinyint(1) NOT NULL DEFAULT 0,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `tb_transporters`
--

INSERT INTO `tb_transporters` (`idtransporter`, `idpagarme`, `idplan`, `tokenssearchused`, `countmytokens`, `destransporter`, `descnpj`, `descep`, `descity`, `desstreet`, `desneighborhood`, `desstreetnumber`, `nrphonetransporter`, `desemailtransporter`, `desrole`, `desimg`, `deschangebenefit`, `deslimitsearch`, `state_read`, `firstaccess`, `dtregister`) VALUES
(1, 4566917, 0, '0', '0', 'RAMOS SENA E BENTO LTDA', '09.600.944/0001-74', '78.750-410', 'Rondonópolis', 'Rua Raul Pompéia', 'Jardim Rui Barbosa', '2123', '(66) 9965-88337', 'sjtrlog@gmail.com', 'Proprietario', '/views/assets/img/transporter/619d6c852a419.jpg', '50', '500', 0, 0, '2021-10-25 19:52:09'),
(52, 5194052, 0, '18', '52', 'Transportadora Rio Branco', '57768429000140', '03875000', 'São Paulo', 'Rua Sancho Junqueira', 'Vila Rio Branco', '51', '11986569023', 'denilsondias59@gmail.com', '', '/views/assets/img/transporter/621e546a2e43c.jpg', '100', '400', 0, 1, '2022-03-01 14:57:34');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_transportersdrivers`
--

CREATE TABLE `tb_transportersdrivers` (
  `id_transporters_drivers` int(11) NOT NULL,
  `idtransporter` int(11) DEFAULT NULL,
  `iddriver` int(11) DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_transportersdrivers`
--

INSERT INTO `tb_transportersdrivers` (`id_transporters_drivers`, `idtransporter`, `iddriver`, `dtregister`) VALUES
(331, 52, 282, '2022-03-05 00:44:46'),
(332, 52, 283, '2022-03-11 13:13:38'),
(333, 53, 284, '2022-03-11 13:13:38'),
(334, 52, 282, '2022-03-05 00:44:46'),
(335, 52, 283, '2022-03-11 13:13:38'),
(336, 53, 284, '2022-03-11 13:13:38'),
(337, 52, 282, '2022-03-05 00:44:46'),
(338, 52, 283, '2022-03-11 13:13:38'),
(339, 53, 284, '2022-03-11 13:13:38'),
(340, 52, 282, '2022-03-05 00:44:46'),
(341, 52, 283, '2022-03-11 13:13:38'),
(342, 53, 284, '2022-03-11 13:13:38'),
(343, 52, 282, '2022-03-05 00:44:46'),
(344, 52, 283, '2022-03-11 13:13:38'),
(345, 53, 284, '2022-03-11 13:13:38'),
(346, 52, 282, '2022-03-05 00:44:46'),
(347, 52, 283, '2022-03-11 13:13:38'),
(348, 53, 284, '2022-03-11 13:13:38'),
(349, 52, 282, '2022-03-05 00:44:46'),
(350, 52, 283, '2022-03-11 13:13:38'),
(351, 53, 284, '2022-03-11 13:13:38'),
(352, 52, 282, '2022-03-05 00:44:46'),
(353, 52, 283, '2022-03-11 13:13:38'),
(354, 53, 284, '2022-03-11 13:13:38'),
(355, 52, 282, '2022-03-05 00:44:46'),
(356, 52, 283, '2022-03-11 13:13:38'),
(357, 53, 284, '2022-03-11 13:13:38'),
(358, 52, 282, '2022-03-05 00:44:46'),
(359, 52, 283, '2022-03-11 13:13:38'),
(360, 53, 284, '2022-03-11 13:13:38'),
(361, 52, 282, '2022-03-05 00:44:46'),
(362, 52, 283, '2022-03-11 13:13:38'),
(363, 53, 284, '2022-03-11 13:13:38'),
(364, 52, 282, '2022-03-05 00:44:46'),
(365, 52, 283, '2022-03-11 13:13:38'),
(366, 53, 284, '2022-03-11 13:13:38'),
(367, 52, 282, '2022-03-05 00:44:46'),
(368, 52, 283, '2022-03-11 13:13:38'),
(369, 53, 284, '2022-03-11 13:13:38'),
(370, 52, 282, '2022-03-05 00:44:46'),
(371, 52, 283, '2022-03-11 13:13:38'),
(372, 53, 284, '2022-03-11 13:13:38'),
(373, 52, 282, '2022-03-05 00:44:46'),
(374, 52, 283, '2022-03-11 13:13:38'),
(375, 53, 284, '2022-03-11 13:13:38'),
(376, 52, 282, '2022-03-05 00:44:46'),
(377, 52, 283, '2022-03-11 13:13:38'),
(378, 53, 284, '2022-03-11 13:13:38'),
(379, 52, 282, '2022-03-05 00:44:46'),
(380, 52, 283, '2022-03-11 13:13:38'),
(381, 53, 284, '2022-03-11 13:13:38'),
(382, 52, 282, '2022-03-05 00:44:46'),
(383, 52, 282, '2022-03-05 00:44:46'),
(384, 52, 283, '2022-03-11 13:13:38'),
(385, 53, 284, '2022-03-11 13:13:38'),
(386, 52, 282, '2022-03-05 00:44:46'),
(387, 52, 283, '2022-03-11 13:13:38'),
(388, 53, 284, '2022-03-11 13:13:38'),
(389, 52, 282, '2022-03-05 00:44:46'),
(390, 52, 283, '2022-03-11 13:13:38'),
(391, 53, 284, '2022-03-11 13:13:38'),
(392, 52, 282, '2022-03-05 00:44:46'),
(393, 52, 283, '2022-03-11 13:13:38'),
(394, 53, 284, '2022-03-11 13:13:38'),
(395, 52, 282, '2022-03-05 00:44:46'),
(396, 52, 283, '2022-03-11 13:13:38'),
(397, 53, 284, '2022-03-11 13:13:38'),
(398, 52, 282, '2022-03-05 00:44:46'),
(399, 52, 283, '2022-03-11 13:13:38'),
(400, 53, 284, '2022-03-11 13:13:38'),
(401, 52, 282, '2022-03-05 00:44:46'),
(402, 52, 283, '2022-03-11 13:13:38'),
(403, 53, 284, '2022-03-11 13:13:38'),
(404, 52, 282, '2022-03-05 00:44:46'),
(405, 52, 283, '2022-03-11 13:13:38'),
(406, 53, 284, '2022-03-11 13:13:38'),
(407, 52, 282, '2022-03-05 00:44:46');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_travel_driver`
--

CREATE TABLE `tb_travel_driver` (
  `idtraveldriver` int(11) NOT NULL,
  `namedriver` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nrphone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cpfdriver` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datereference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typecargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nametransporter` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `board` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pricecargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_travel_driver`
--

INSERT INTO `tb_travel_driver` (`idtraveldriver`, `namedriver`, `nrphone`, `cpfdriver`, `datereference`, `typecargo`, `nametransporter`, `board`, `pricecargo`, `dtregister`) VALUES
(37, 'Denilson Dias', '11986569023', '43060973822', '2021-05-01', 'Arroz', 'Transportadora Speed', 'SPW2982', 'R$ 80.000,00', '2021-11-05 20:20:18'),
(39, 'Denilson Dias', '11986569023', '43060973822', '2021-08-09', 'Feijão', 'Transportadora Loggitech', 'PSC2778', 'R$ 110.000,00', '2021-11-05 20:20:18'),
(40, 'Vitor Santos', '11958627865', '37968937400', '01/05/2021', 'Arroz', 'Transportadora Speed', 'SPW2982', 'R$ 80.000,00', '2021-11-17 12:06:11'),
(41, 'Vitor Santos', '11958627865', '37968937400', '09/03/2021', 'Trigo', 'Transportadora Rio Branco', 'TUZ2432', 'R$ 90.000,00', '2021-11-17 12:06:11'),
(42, 'Vitor Santos', '11958627865', '37968937400', '08/09/2021', 'Feijão', 'Transportadora Loggitech', 'PSC2778', 'R$ 110.000,00', '2021-11-17 12:06:11'),
(43, 'Rodrigo Fernandes Malagutti', '66996262229', '27256384882', '01/10/2021', 'Soja', 'SJ Transportes e Logística', 'AAA0001', 'R$ 100.000,0', '2021-11-20 12:56:34'),
(44, 'Rodrigo Fernandes Malagutti', '66996262229', '27256384882', '01/09/2021', 'Milho', 'G10', 'BBB0001', 'R$ 200.000,0', '2021-11-20 12:56:34'),
(45, 'Rodrigo Fernandes Malagutti', '66996262229', '27256384882', '01/08/2021', 'Algodao', 'Fribon', 'AAA0002', 'R$ 300.000,0', '2021-11-20 12:56:34'),
(46, 'Rodrigo Fernandes Malagutti', '66996262229', '27256384882', '01/07/2021', 'Fertilizante', 'RDM', 'BBB0002', 'R$ 400.000,0', '2021-11-20 12:56:34'),
(47, 'Denilson Dias', '11986569023', '43060973822', '2021-08-09', 'Trigo', 'Transportadora Loggitech', 'PSC2778', 'R$ 150.000,00', '2021-11-05 20:20:18');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_users`
--

CREATE TABLE `tb_users` (
  `iduser` int(11) NOT NULL,
  `idperson` int(11) DEFAULT NULL,
  `idtransporter` int(11) DEFAULT NULL,
  `iddriver` int(11) DEFAULT NULL,
  `deslogin` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `despassword` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `permission` tinyint(4) DEFAULT NULL,
  `attempts_login` tinyint(4) NOT NULL,
  `dattempts_login` int(11) NOT NULL,
  `inadmin` tinyint(4) DEFAULT 0,
  `read_notifications` tinyint(4) DEFAULT 0,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Despejando dados para a tabela `tb_users`
--

INSERT INTO `tb_users` (`iduser`, `idperson`, `idtransporter`, `iddriver`, `deslogin`, `despassword`, `permission`, `attempts_login`, `dattempts_login`, `inadmin`, `read_notifications`, `dtregister`) VALUES
(4754, 98, NULL, NULL, 'tokenvalidador@loggitech.log.br', '$2y$12$FBu5IsBrTHJcv7k9ahcROuMHnFZ29jPDvc3EYj8zY.TRBIWztVHUS', NULL, 0, 0, 1, 0, '2021-10-15 23:56:55'),
(4772, NULL, 1, NULL, 'sjtrlog@gmail.com', '$2y$12$idA/x4H9GV9rYebX5DBBtuPjFjsjda7pLo/YC5ZQ1RNcKZflMoA9C', 2, 0, 0, 0, 0, '2021-10-25 19:52:09'),
(4873, NULL, 52, NULL, 'denilsondias59@gmail.com', '$2y$12$OjtuHUe0BITyGwnNsIkEmuY55d0/mxkS8IWWF8F2Ew1BIXc2x/vFK', 2, 0, 0, 0, 0, '2022-03-01 14:57:34'),
(4917, NULL, NULL, 92, 'denidias559@gmail.com', '$2y$12$G624XGiGoMbX735WEm/n/ugIHJd8w8MUxVtS6.iFIHV7YjyatAxfC', 1, 0, 0, 0, 0, '2022-03-03 21:45:54'),
(4919, NULL, NULL, 94, 'enrico_campos@deere.com', '$2y$12$5cnxM9mNHFmu.CFvDhBA/OC0WhmkzEWccKgGZWIY.veKTpAWRXJ6S', 1, 0, 0, 0, 0, '2022-03-03 21:45:55'),
(4920, NULL, NULL, 95, 'denidias559@gmail.com', '$2y$12$uBW1Qu2FIjcg5TaY3z15Ue.O250oNXmTDWgN4dUxKpgAgnJrq3qxu', 1, 0, 0, 0, 0, '2022-03-03 21:45:56'),
(4921, NULL, NULL, 96, 'vinicius.carlos.almeida@hidracom.com.br', '$2y$12$jtkoHwHcY3kQ2G/QqDWAAeTZQU9T4xALH.JVJOj/CjxvU1fwKjQ7K', 1, 0, 0, 0, 0, '2022-03-03 21:45:57'),
(4923, NULL, NULL, 98, 'denidias559@gmail.com', '$2y$12$a4EqTdc0wE.ik1eUJLR8DON.1fOrrmJpN8dFuiEkXe0mz6Hu8cb4q', 1, 0, 0, 0, 0, '2022-03-03 21:57:36'),
(4924, NULL, NULL, 99, 'vinicius.carlos.almeida@hidracom.com.br', '$2y$12$bxsQTxBJSKPbHHIU35h53OvnaKNdE9A.zjnYAuptSubW5jsmBuMQe', 1, 0, 0, 0, 0, '2022-03-03 21:57:37'),
(4925, NULL, NULL, 100, 'enrico_campos@deere.com', '$2y$12$bIN2T/1Uv041lv2HQ5PHguS4eviRAp6Rb9O.4t1TfhR0unKpDe4y.', 1, 0, 0, 0, 0, '2022-03-03 21:57:38'),
(4926, NULL, NULL, 101, 'denidias559@gmail.com', '$2y$12$F4sFYT8CiM42/Tzfyn5kvuMfB9hgq9ECqqs84MXr8mC9.lf7FUtfy', 1, 0, 0, 0, 0, '2022-03-03 21:57:39'),
(4927, NULL, NULL, 102, 'vinicius.carlos.almeida@hidracom.com.br', '$2y$12$JeCiVKh275oG.G97NBVjNuBHdpH8oJQqEEcrRmSp9wR8R.2jRz3XG', 1, 0, 0, 0, 0, '2022-03-03 21:57:40'),
(4928, NULL, NULL, 103, 'enrico_campos@deere.com', '$2y$12$teKSQpynXYZxQo1y2Vp2seDy9Px5t7ycOeblFH3KD/zGOBnjVfQvC', 1, 0, 0, 0, 0, '2022-03-03 21:57:41'),
(5105, NULL, NULL, 282, 'denilsondias277@gmail.com', '$2y$12$4Ec0yyy7eKEuWp1z.yw1buL6744vsAx9zI/80wAVEsiaPlS8ym7pq', 1, 0, 0, 0, 0, '2022-03-05 00:44:46'),
(5106, NULL, NULL, 283, 'ro_malagutti@hotmail.com', '$2y$12$vGQzK76eU4s9oBOOQmjP5.bczywlc374/MLbG5gcDPV2tHNstft.W', 1, 0, 0, 0, 0, '2022-03-11 13:13:38');

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_userslogs`
--

CREATE TABLE `tb_userslogs` (
  `idlog` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `deslog` varchar(128) NOT NULL,
  `desip` varchar(45) NOT NULL,
  `desuseragent` varchar(128) NOT NULL,
  `dessessionid` varchar(64) NOT NULL,
  `desurl` varchar(128) NOT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `tb_userspasswordsrecoveries`
--

CREATE TABLE `tb_userspasswordsrecoveries` (
  `idrecovery` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `desip` varchar(45) NOT NULL,
  `dtrecovery` datetime DEFAULT NULL,
  `dtregister` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `tb_userspasswordsrecoveries`
--

INSERT INTO `tb_userspasswordsrecoveries` (`idrecovery`, `iduser`, `desip`, `dtrecovery`, `dtregister`) VALUES
(120, 4751, '189.103.208.176', NULL, '2021-10-16 12:23:11'),
(121, 4754, '179.100.2.213', NULL, '2021-11-01 17:17:24'),
(122, 4754, '179.100.2.213', '2021-11-01 14:21:34', '2021-11-01 17:21:09'),
(123, 5105, '179.113.106.54', '2022-03-05 19:22:38', '2022-03-05 22:21:58'),
(124, 5105, '179.113.106.54', '2022-03-05 19:23:40', '2022-03-05 22:23:21');

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `tb_cardcredituser`
--
ALTER TABLE `tb_cardcredituser`
  ADD PRIMARY KEY (`id_cardcredit`);

--
-- Índices de tabela `tb_drivers`
--
ALTER TABLE `tb_drivers`
  ADD PRIMARY KEY (`iddriver`);

--
-- Índices de tabela `tb_drivers_general`
--
ALTER TABLE `tb_drivers_general`
  ADD PRIMARY KEY (`iddriver`);

--
-- Índices de tabela `tb_folder`
--
ALTER TABLE `tb_folder`
  ADD PRIMARY KEY (`idfolder`);

--
-- Índices de tabela `tb_folderdrivers`
--
ALTER TABLE `tb_folderdrivers`
  ADD PRIMARY KEY (`idfolderdrivers`);

--
-- Índices de tabela `tb_folders_backup`
--
ALTER TABLE `tb_folders_backup`
  ADD PRIMARY KEY (`idfolder`),
  ADD UNIQUE KEY `idfolder` (`idfolder`),
  ADD UNIQUE KEY `desname` (`desname`);

--
-- Índices de tabela `tb_notifications_services`
--
ALTER TABLE `tb_notifications_services`
  ADD PRIMARY KEY (`idnotification`);

--
-- Índices de tabela `tb_persons`
--
ALTER TABLE `tb_persons`
  ADD PRIMARY KEY (`idperson`);

--
-- Índices de tabela `tb_plans`
--
ALTER TABLE `tb_plans`
  ADD PRIMARY KEY (`idplan`);

--
-- Índices de tabela `tb_planuser`
--
ALTER TABLE `tb_planuser`
  ADD PRIMARY KEY (`idplanuser`);

--
-- Índices de tabela `tb_reset_tokens`
--
ALTER TABLE `tb_reset_tokens`
  ADD PRIMARY KEY (`idreset_tokens`);

--
-- Índices de tabela `tb_transporters`
--
ALTER TABLE `tb_transporters`
  ADD PRIMARY KEY (`idtransporter`);

--
-- Índices de tabela `tb_transportersdrivers`
--
ALTER TABLE `tb_transportersdrivers`
  ADD PRIMARY KEY (`id_transporters_drivers`);

--
-- Índices de tabela `tb_travel_driver`
--
ALTER TABLE `tb_travel_driver`
  ADD PRIMARY KEY (`idtraveldriver`);

--
-- Índices de tabela `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `idtransporter` (`idtransporter`),
  ADD KEY `fk_users_persons` (`idperson`),
  ADD KEY `fk_users_drivers` (`iddriver`) USING BTREE;

--
-- Índices de tabela `tb_userslogs`
--
ALTER TABLE `tb_userslogs`
  ADD PRIMARY KEY (`idlog`),
  ADD KEY `fk_userslogs_users_idx` (`iduser`);

--
-- Índices de tabela `tb_userspasswordsrecoveries`
--
ALTER TABLE `tb_userspasswordsrecoveries`
  ADD PRIMARY KEY (`idrecovery`),
  ADD KEY `fk_userspasswordsrecoveries_users_idx` (`iduser`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tb_cardcredituser`
--
ALTER TABLE `tb_cardcredituser`
  MODIFY `id_cardcredit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `tb_drivers`
--
ALTER TABLE `tb_drivers`
  MODIFY `iddriver` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;

--
-- AUTO_INCREMENT de tabela `tb_drivers_general`
--
ALTER TABLE `tb_drivers_general`
  MODIFY `iddriver` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `tb_folder`
--
ALTER TABLE `tb_folder`
  MODIFY `idfolder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `tb_folderdrivers`
--
ALTER TABLE `tb_folderdrivers`
  MODIFY `idfolderdrivers` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_folders_backup`
--
ALTER TABLE `tb_folders_backup`
  MODIFY `idfolder` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT de tabela `tb_notifications_services`
--
ALTER TABLE `tb_notifications_services`
  MODIFY `idnotification` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT de tabela `tb_persons`
--
ALTER TABLE `tb_persons`
  MODIFY `idperson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT de tabela `tb_plans`
--
ALTER TABLE `tb_plans`
  MODIFY `idplan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de tabela `tb_planuser`
--
ALTER TABLE `tb_planuser`
  MODIFY `idplanuser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `tb_reset_tokens`
--
ALTER TABLE `tb_reset_tokens`
  MODIFY `idreset_tokens` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de tabela `tb_transporters`
--
ALTER TABLE `tb_transporters`
  MODIFY `idtransporter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de tabela `tb_transportersdrivers`
--
ALTER TABLE `tb_transportersdrivers`
  MODIFY `id_transporters_drivers` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=408;

--
-- AUTO_INCREMENT de tabela `tb_travel_driver`
--
ALTER TABLE `tb_travel_driver`
  MODIFY `idtraveldriver` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de tabela `tb_users`
--
ALTER TABLE `tb_users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5107;

--
-- AUTO_INCREMENT de tabela `tb_userslogs`
--
ALTER TABLE `tb_userslogs`
  MODIFY `idlog` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tb_userspasswordsrecoveries`
--
ALTER TABLE `tb_userspasswordsrecoveries`
  MODIFY `idrecovery` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=125;

--
-- Restrições para tabelas despejadas
--

--
-- Restrições para tabelas `tb_users`
--
ALTER TABLE `tb_users`
  ADD CONSTRAINT `fk_users_persons` FOREIGN KEY (`idperson`) REFERENCES `tb_persons` (`idperson`);

--
-- Restrições para tabelas `tb_userslogs`
--
ALTER TABLE `tb_userslogs`
  ADD CONSTRAINT `fk_userslogs_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `tb_userspasswordsrecoveries`
--
ALTER TABLE `tb_userspasswordsrecoveries`
  ADD CONSTRAINT `fk_userspasswordsrecoveries_users` FOREIGN KEY (`iduser`) REFERENCES `tb_users` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
