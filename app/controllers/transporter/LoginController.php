<?php

namespace app\controllers\transporter;

use app\controllers\Controller;
use app\models\transporter\Transporter;
use app\src\Validate;
use app\src\Login;

class LoginController extends Controller {

    public function index() {
        $this->view('transporter.login', []);
    }

    public function store() {
        $validate = new Validate;
        $data = $validate->validate([
            'deslogin' => 'required',
            'password' => 'required'
        ]);

        if($validate->hasErrors()){
            return back();
        }

        $login = new Login('transporter');
        $loggedIn = $login->login($data, new Transporter);
       
        if(!$loggedIn){
            flash('message', error('Erro ao logar, permissão, E-mail ou senha inválidos!'));

            return back();
        }

        redirect('/update-tokenvalidador/public/transporter/painel');
    }

}