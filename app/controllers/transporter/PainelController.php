<?php

namespace app\controllers\transporter;

use app\controllers\Controller;
use app\models\Drivers;
use app\models\transporter\Transporter;
use app\src\Validate;
use app\src\Login;
use app\models\admin\Admin;

use app\src\Pagarme;

class PainelController extends Controller {

    private $data;

    public function index() {

        $transporter = new Transporter;
            
        $quantitydrivers        = $transporter->quantDrivers();
        $tokensvalidate         = $transporter->tokensValidate(); 
            
        $mytokens               = $transporter->myContractTokens();
        $mytotaltokens          = $transporter->myTotalTokens();
        
        $mytokenssearch         = $transporter->myTokensSearchUsed();
        $mytotaltokenssearch    = $transporter->myTotalTokensSearch();

        $showlastdrivers        = $transporter->myDrivers();

        $this->view('transporter.painel', [
            'title'                 => 'Painel Administrativo Transportadora',
            'quantitydrivers'       => $quantitydrivers,
            'tokensvalidate'        => $tokensvalidate,
            'countmytokens'         => $mytokens,
            'deschangebenefit'      => $mytotaltokens,
            'showlastdrivers'       => $showlastdrivers,
            'mytokenssearch'        => $mytokenssearch,
            'mytotaltokenssearch'   => $mytotaltokenssearch
        ]);
    }

    public function backpSearch() {

        $this->view('transporter.search', [
            'title' => 'Backup de Pesquisas'
        ]);

    }

    public function account () {
          
        $transporter = new Transporter;
        $dataUser = $transporter->user();

        $this->view('transporter.account', [
            'title' => 'Minha Conta',
            'data'  => $dataUser
        ]);

    }

    public function accountUpdate() {


        $validate = new Validate;
        $data = $validate->validate([
            'destransporter'        => 'required:max@100',
            'desemailtransporter'   => 'required:email',
            'descnpj'               => 'required',
            'descep'                => 'required',
            'descity'               => 'required',
            'desneighborhood'       => 'required',
            'desstreet'             => 'required',
            'nrphonetransporter'    => 'required:phone',
            'desrole'               => 'required',
            'desstreetnumber'       => 'required'
        ]);

        if($validate->hasErrors()) {
            return back();
        }





    }

    public function drivers () {
        $transporter = new Transporter;

        $dataUser = $transporter->drivers();

        dd($dataUser);

        $this->view('transporter.drivers', [
            'title' => 'Meus Motoristas',
            'links' => $transporter->links(),
            'data'  => $dataUser
        ]);

    }

    public function show() {

    }

    public function create() {

    }

    public function store() {

    }

    public function edit() {

    }

    public function update() {

    }

    public function destroy() {
        
        $login = new Login('transporter');
        return $login->logout();
    }

}