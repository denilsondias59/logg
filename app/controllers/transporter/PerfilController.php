<?php

namespace app\controllers\transporter;

use app\controllers\Controller;

class PerfilController extends Controller {

    public function index() {

        $this->view('transporter.account', [
            'title' => 'Minha Conta'
        ]);

    }

}