<?php

namespace app\controllers;

use app\src\Image;
use app\src\Flash;
use app\src\Validate;

use app\models\Users;


class PerfilPhotoController extends Controller {

  

    public function store() {

        $validate = new Validate();
        $validate->validate([
            'file' => 'image'
        ]);

        if($validate->hasErrors()) {
            return back();
        }

        $user = new Users;
        $userEncontrado = $user->select()->where('id', 1)->first();
        
        
        $image = new Image('file');
        $image->delete($userEncontrado->photo);
        $image->size('user')->upload();

        $user->find('id', 1)->update([
            'photo' => "/curso-twig/public/assets/images/photos/{$image->getName()}",
        ]);

        
        flash('message', success('Imagem Cadastrada com sucesso!'));
        return back();
        

    }

}