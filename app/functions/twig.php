<?php

use app\src\Flash;
use app\models\transporter\Transporter;
use app\src\Pagarme;

$message = new \Twig_SimpleFunction('message', function($index) {

    echo Flash::get($index);

});

$transporter = new \Twig_SimpleFunction('transporter', function() {

    return Transporter::getUser();

});

$pagarme = new \Twig_SimpleFunction('pagarme', function() {

    $pagarme = new Pagarme;
    return $pagarme->statusPayment();

});


return [
    $message, $transporter, $pagarme
];