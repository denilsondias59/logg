<?php

namespace app\models\transporter;

use app\models\Model;
use app\src\Login;

class Transporter extends Model {
    
    protected $table = 'tb_users';
    protected $tbdrivers = 'tb_drivers';
    protected $tbtransporters = 'tb_transporters';
    protected $tbtransportersdrivers = 'tb_transportersdrivers';
    protected $tbnotificationsservices = 'tb_notifications_services';

    public function user() {

        if(!isset($_SESSION['id_transporter'])) {
            throw new \Exception("Você não pode acessar essa página");
        }

        $id = $_SESSION['id_transporter'];

        $this->sql = "select * from {$this->tbtransporters}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function transporter() {
        $id = $this->user()->idtransporter;

        $this->sql = "select * from {$this->table}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

      
    public function quantDrivers() {

        $id = $this->user()->idtransporter;

        $this->sql = "SELECT COUNT(*) AS nrtotal FROM {$this->tbtransportersdrivers}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function tokensValidate() {

        $id = $this->user()->idtransporter;

        $this->sql = "SELECT COUNT(*) AS nrtotal FROM {$this->tbnotificationsservices}";

        $this->where('idtransporter', $id);

        return $this->first();

    }

    public function myContractTokens() {

        $id = $this->user()->idtransporter;

        $this->sql = "select countmytokens from {$this->tbtransporters}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function myTotalTokens() {

        $id = $this->user()->idtransporter;

        $this->sql = "select deschangebenefit from {$this->tbtransporters}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function myTokensSearchUsed() {

        $id = $this->user()->idtransporter;

        $this->sql = "select tokenssearchused from {$this->tbtransporters}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function myTotalTokensSearch() {

        $id = $this->user()->idtransporter;

        $this->sql = "select deslimitsearch from {$this->tbtransporters}";

        $this->where('idtransporter', $id);

        return $this->first();
    }

    public function myDrivers() {

        $id = $this->user()->idtransporter;
        
        $this->sql = "SELECT * FROM {$this->tbdrivers} a INNER JOIN {$this->tbtransportersdrivers} b ON a.iddriver = b.iddriver where b.idtransporter = {$id}";

        $this->order('b.idtransporter', 'DESC');
        
        $this->limit(5);

        return $this->get();
    
    }

    public function drivers() {

        $id = $this->user()->idtransporter;

        $this->sql = "SELECT FROM * {$this->tbdrivers} a INNER JOIN {$this->tbtransportersdrivers} b on a.idtransporter = b.idtransporter";

        $this->where('b.idtransporter', $id);

        $this->paginate(30);

        $this->get();
    }

}






