<?php

namespace app\models;

use app\traits\Create;
use app\traits\Read;
use app\traits\Update;
use app\traits\Delete;
use app\models\Connection;

abstract class Model {

    use Create, Read, Update, Delete;

    protected $connect;
    protected $field;
    protected $value;

    public function __construct() {
        $this->connect = Connection::connect();
    }

    public function all() {
        $sql = "SELECT * FROM {$this->table}";
        $all = $this->connect->query($sql);
        $all->execute();

        return $all->fetchAll();
    }

    public function find($field, $value) {
        $this->field = $field;
        $this->value = $value;

        return $this;
    }

    public function destroy($field, $value){
        $sql =  "DELETE FROM {$this->table} WHERE {$field} = :{$field}";
        $delete = $this->connect->prepare($sql);
        $delete->bindValue($field, $value);
        $delete->execute();

        return $delete->rowCount();
    }

}