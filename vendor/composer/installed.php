<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'container-interop/container-interop' => array(
            'pretty_version' => '1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../container-interop/container-interop',
            'aliases' => array(),
            'reference' => '79cbf1341c22ec75643d841642dd5d6acd83bdb8',
            'dev_requirement' => false,
        ),
        'dopesong/slim-whoops' => array(
            'pretty_version' => '2.3.0',
            'version' => '2.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dopesong/slim-whoops',
            'aliases' => array(),
            'reference' => '59d6f0a6e4368927a2f2a67e98e47f17962b3310',
            'dev_requirement' => false,
        ),
        'filp/whoops' => array(
            'pretty_version' => '2.14.5',
            'version' => '2.14.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../filp/whoops',
            'aliases' => array(),
            'reference' => 'a63e5e8f26ebbebf8ed3c5c691637325512eb0dc',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.4.2',
            'version' => '7.4.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => 'ac1ec1cd9b5624694c3a40be801d94137afb12b4',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.2.1',
            'version' => '2.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => 'c94a94f120803a18554c1805ef2e539f8285f9a2',
            'dev_requirement' => false,
        ),
        'nikic/fast-route' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nikic/fast-route',
            'aliases' => array(),
            'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
            'dev_requirement' => false,
        ),
        'pagarme/pagarme-php' => array(
            'pretty_version' => 'v4.1.2',
            'version' => '4.1.2.0',
            'type' => 'lib',
            'install_path' => __DIR__ . '/../pagarme/pagarme-php',
            'aliases' => array(),
            'reference' => 'a70d32904369d9fad1afc9c8ef998d4c785c2947',
            'dev_requirement' => false,
        ),
        'pimple/pimple' => array(
            'pretty_version' => 'v3.5.0',
            'version' => '3.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../pimple/pimple',
            'aliases' => array(),
            'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'slim/slim' => array(
            'pretty_version' => '3.12.1',
            'version' => '3.12.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../slim/slim',
            'aliases' => array(),
            'reference' => 'eaee12ef8d0750db62b8c548016d82fb33addb6b',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '30885182c981ab175d4d034db0f6f469898070ab',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'twig/twig' => array(
            'pretty_version' => 'v2.14.11',
            'version' => '2.14.11.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../twig/twig',
            'aliases' => array(),
            'reference' => '66baa66f29ee30e487e05f1679903e36eb01d727',
            'dev_requirement' => false,
        ),
    ),
);
