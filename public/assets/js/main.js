function togglemenudesktop() {
	let toggle = document.querySelector('.show-hide-menu');
	let navigation = document.querySelector(".sidebar");
	let content = document.querySelector(".content");
	toggle.classList.toggle('active');
	navigation.classList.toggle("active");
	content.classList.toggle("active");
}

/* Define constant menu mobile panel admin */
const navMenu = document.getElementById('sidebar'),
	navToggle = document.getElementById('menu_mobile'),
	navClose = document.getElementById('hide_menu_mobile')

/*===== MENU SHOW =====*/
/* Validate if constant exists */
if (navToggle) {
	navToggle.addEventListener('click', () => {
		navMenu.classList.add('active')
	})
}

/*===== MENU HIDDEN =====*/
/* Validate if constant exists */
if (navClose) {
	navClose.addEventListener('click', () => {
		navMenu.classList.remove('active')
	})
}

/* Define constant menu mobile panel admin */
const navMenuSite = document.getElementById('menu'),
	navToggleSite = document.getElementById('icon_show_menu'),
	navCloseSite = document.getElementById('icon_hide_menu')

/*===== MENU SHOW =====*/
/* Validate if constant exists */
if (navToggleSite) {
	navToggleSite.addEventListener('click', () => {
		navMenuSite.classList.add('active_menu');
		navToggleSite.style.display = 'none';
		navCloseSite.style.display = 'flex';

		//let hidden = document.querySelector("body");
		//hidden.style.overflow = 'hidden';

	})

}

/*===== MENU HIDDEN =====*/
/* Validate if constant exists */
if (navCloseSite) {
	navCloseSite.addEventListener('click', () => {
		navMenuSite.classList.remove('active_menu');
		navCloseSite.style.display = 'none';
		navToggleSite.style.display = 'flex';

	})
}

function dataexpiration() {
	$("#card_expiration_month").keydown(function() {
		try {
			$("#document_number").unmask();
		} catch (e) {}

		var tamanho = $("#document_number").val().length;

		if (tamanho < 11) {
			$("#document_number").mask("999.999.999-99");
		} else {
			$("#document_number").mask("99.999.999/9999-99");
		}

		// ajustando foco
		var elem = this;
		setTimeout(function() {
			// mudo a posição do seletor
			elem.selectionStart = elem.selectionEnd = 10000;
		}, 0);
		// reaplico o valor para mudar o foco
		var currentValue = $(this).val();
		$(this).val('');
		$(this).val(currentValue);
	});

	$("#card_expiration_month").keydown(function() {
		try {
			$("#card_expiration_month").unmask();
		} catch (e) {}

		var tamanho = $("#card_expiration_month").val().length;

		if (tamanho < 4) {
			$("#card_expiration_month").mask("99/99");
		} else {
			$("#card_expiration_month").mask("99/9999");
		}

		// ajustando foco
		var elem = this;
		setTimeout(function() {
			// mudo a posição do seletor
			elem.selectionStart = elem.selectionEnd = 10000;
		}, 0);
		// reaplico o valor para mudar o foco
		var currentValue = $(this).val();
		$(this).val('');
		$(this).val(currentValue);
	});
}

function masknumber() {
	$("#card_number").mask("9999 9999 9999 9999");
}

$(".upload_form_csv").submit(function(e) {
	e.preventDefault();

	var form = $(this);
	var div_success = $(".bar");
	var div_bar = $(".progress-bar");
	var div_error = $("alert-danger");
	var send_success = $(".alert-danger-success");
	var send_fail = $(".alert-danger-error");

	$.ajax({
		xhr: function() {
			var xhr = new window.XMLHttpRequest();

			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {

					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);

					div_success.css("display", "flex");

					div_bar.css("display", "block");
					$(".progress").html(percentComplete + '%');
					$(".progress").width(percentComplete + '%');

				}
			}, false);

			return xhr;

		},
		type: 'post',
		url: '/transporter/drivers/import',
		data: new FormData(this),
		contentType: false,

		cache: false,
		processData: false,
		success: function(data) {
			console.log(data)
			let result = data;
			$(".message-alert").html('<div class="alert alert-danger-success"><i class="ri-error-warning-fill error-msg"></i>' + data + '</div>');
			// send_success.fadeIn();

		},
		error: function(data) {
		},
		complete: function(data) {

		},
	});

});

let elements = $("tbody tr").length;

if (elements <= 0) {
	$(".table-historic").hide();
}

$("#formGroupExampleInputData").mask("99/99/9999");

$("#formGroupExampleInputdata").mask("99/99/9999");

$("#formGroupExampleInputCpf,.image_cnh_form").mask("999.999.999-99");

$('#searchtravel').mask('999.999.999-99');
$('#formGroupExampleInputCpfdpf').mask('999.999.999-99');

$(".msg_warnning.alert.alert-warning").hide();

$(".msg_warnning_cpf").hide();
$(".animate_cpf").hide();

$(".error.alert.alert-danger").hide();

$(".search-cpf").submit(function(e) {
	$(".animate_cpf").show();
	$(".result_cpf ").hide();
	$(".msg_warnning_cpf.alert.alert-warning").hide();
	e.preventDefault();

	$(".nome div").html("");
	$(".consulta_comprovante div").html("");
	$(".consulta_datahora div").html("");
	$(".cpf_consulta div").html("");
	$(".data_inscricao div").html("");
	$(".data_nascimento div").html("");
	$(".site_receipt div").html("");
	$(".situacao_cadastral div").html("");

	var data = $("#formGroupExampleInputData").val();
	var cpfs = $("#formGroupExampleInputCpf").val();

	let msgErrorFields = "Preencha todos os campos para continuar!";

	if (data == '' || cpfs == '') {

		$(".error.alert.alert-danger").show();
		$(".animate_cpf").hide();
		$(".error.alert.alert-danger").html(msgErrorFields);
		$(".msg_warnning_cpf.alert.alert-warning").hide();
		return;

	}

	$(".error.alert.alert-danger").hide();

	var showLoader = false;

	var timeoutLoader = setTimeout(function() {
		showLoader = true;
		$(".load").show();
	}, 2000); // 2000ms = 2 segundos

	axios({
			method: "post",
			url: "/transporter/drivers/search/cpf",
			data: {
				data: data,
				cpf: cpfs
			},
		})
		.then(function(response) {

			$(".animate_cpf").hide();

			let itemObj = response["data"];

			let msgNotExistis = "Não foram encontrados dados, verifique os dados inseridos!";

			if (response["data"] == "" || itemObj == null || itemObj["data"] == msgNotExistis) {
				$(".col-12.mt-5.msg_warnning_cpf.alert.alert-warning").fadeIn();
				$(".col-12.mt-5.msg_warnning_cpf.alert.alert-warning").html(msgNotExistis);
				$(".result_dpf").hide();

				$(".error.alert.alert-danger").hide();
				return;
			}

			$(".result_cpf").show();

			let nome = itemObj[0]["nome"];

			let consulta_comprovante = itemObj[0]["consulta_comprovante"];
			let consulta_datahora = itemObj[0]["consulta_datahora"];
			let cpf = itemObj[0]["cpf"];
			let data_inscricao = itemObj[0]["data_inscricao"];
			let data_nascimento = itemObj[0]["data_nascimento"];
			let site_receipt = itemObj[0]["site_receipt"];
			let situacao_cadastral = itemObj[0]["situacao_cadastral"];

			axios.post('/transporters/savepdf/cpf', {
					nome: nome,
					consulta_comprovante: consulta_comprovante,
					consulta_datahora: consulta_datahora,
					cpf: cpf,
					data_inscricao: data_inscricao,
					data_nascimento: data_nascimento,
					site_receipt: site_receipt,
					situacao_cadastral: situacao_cadastral
				})
				.then(function(response) {
				})
				.catch(function(error) {
				});

			if (itemObj == "") {
				$(".animate_cpf").hide();
				$(".result_cpf").hide();
				$(".msg_warnning_cpf.alert.alert-warning").hide();
				$(".error.alert.alert-danger").html(msgError);
				$(".error.alert.alert-danger").show();

			} else {
				$(".error.alert.alert-danger").hide();
				$(".msg_warnning_cpf.alert.alert-warning").hide();
				$(".animate_cpf").hide();

			}

			$(".nome div").html(nome);
			consulta_comprovante
			$(".consulta_comprovante div").html(consulta_comprovante);
			$(".consulta_datahora div").html(consulta_datahora);
			$(".cpf_consulta div").html(cpf);
			$(".data_inscricao div").html(data_inscricao);
			$(".data_nascimento div").html(data_nascimento);
			$(".site_receipt div").html("<a class='btn-info p-2' href='" + site_receipt + "'>Clique para acessar</a>");
			$(".situacao_cadastral div").html(situacao_cadastral);

			$(".download_pdf_cpf").html("<a class='download-pdf-search' href='/transporter/search/generate-pdf/cpf/" + cpfs + "/birthdate/" + data_nascimento.replace(/\//g, '') + "'>Baixar PDF</a>")
			$(".result_cpf").show();

			if ($(".nome div, .consulta_comprovante div, .consulta_datahora div, .cpf div, .data_inscricao div, .data_nascimento div, .site_receipt div, .situacao_cadastral div").html().length > 0) {
				$(".result_cpf").show();
				$(".error p").hide();
			}

		})

		.catch(function(response) {

			$(".animate_cpf").hide();

		})

	// let cpfFormat = cpfs.replace('.','').replace('-','').replace('.','') ;
	// let dateFormat = data.replace('/','-').replace('/', '-');

	// axios.get('/transporters/savepdf/cpf/'+cpfFormat+'/'+dateFormat)
	// .then(function(response){

	// })
	return false;
});

$('.result_cnh.mt-5').hide();
$(".error_cnh.alert.alert-danger").hide();
$(".msg_warnning_cnh.alert.alert-warning").hide();
$(".animate_cnh").hide();

$(".input_cnh_cpf").mask("999.999.999-99");
$(".input_cnh_date, .input_cnh_date_first, .input_cnh_validatecnh, .input_cnh_lastemission").mask("99/99/9999");

$(".input_cnh_date, .input_cnh_date_first, .input_cnh_validatecnh, .input_cnh_lastemission").on("input", function() {
	var regexp = /[^0-9]+$/g;
	if (this.value.match(regexp)) {
		$(this).val(this.value.replace(regexp, ''));
	}
});

$(".input_cnh_date").on("input", function() {
	var regexp = /[^0-9]+$/g;
	if (this.value.match(regexp)) {
		$(this).val(this.value.replace(regexp, ''));
	}
});

$(".input_cnh_numberdocument").on("input", function() {
	var regexp = /[^a-zA-Z0-9.-]/g;
	if (this.value.match(regexp)) {
		$(this).val(this.value.replace(regexp, ''));
	}
});

$(".input_cnh_name, .name_mother, .input_cnh_namefather, .input_cnh_orgaodocument, .input_cnh_ufexpedidor, .input_cnh_obs_cnh, .input_cnh_imp").on("input", function() {
	var regexp = /[^a-zA-Zíéáóéãâ ']/g;
	if (this.value.match(regexp)) {
		$(this).val(this.value.replace(regexp, ''));
	}
});

$(".form-input-image-cnh button").click(function() {
	$(".biometria_probabilidade span p").html("");
	$(".biometria_similaridade span p").html("");
	$(".categoria_cnh span p").html("");
	$(".codigo_situacao_cnh span p").html("");
	$(".data_primeira_habilitacao span p").html("");
	$(".ultima_emissao span p").html("");
	$(".data_de_validade span p").html("");
	$(".nome_cnh span p").html("");
	$(".similaridade_nome_cnh span p").html("");
	$(".num_registro span p").html("");
	$(".num_documento_similaridade span p").html("");
	$(".documento_uf_expedidor span p").html("");
	$(".nome_pai span p").html("");
	$(".nome_mae span p").html("");
});


$(".form-input-image-cnh").submit(function(e) {

	e.preventDefault();

	$(".result_cnh").hide();
	$(".animate_cnh").show();
	$(".msg_warnning_cnh.alert.alert-warning").hide();

	var bodyFormData = new FormData($("form[name='form-cnh']")[0]);

	let field = $(".image_cnh").val();

	let cnh_cpf = $(".input_cnh_cpf").val();
	let image_biometer = $(".image_cnh_form").val();

	let msgErro = "Preencha todos os campos para continuar!";

	if (!cnh_cpf || cnh_cpf == '' && !image_biometer || image_biometer == '') {
		$(".animate_cnh").hide();
		$(".error_cnh.alert.alert-danger.mt-5").fadeIn();
		$(".error_cnh.alert.alert-danger.mt-5").html(msgErro);
		$(".result_cnh").hide();
		return
	}

	$(".error_cnh.alert.alert-danger.mt-5").hide();

	axios({
			method: "post",
			url: "/transporter/drivers/search/cnh",
			data: bodyFormData,
			headers: {
				"Content-Type": "multipart/form-data"
			},
		})
		.then(function(response) {
			$(".animate_cnh").hide();

			let data = response["data"];

			let msgToken = "Seus tokens para pesquisa expiraram!";
			let msgNotExistis = "Não foram encontrados dados desse CPF!";

			if (data == "" || response == null) {
				$(".msg_warnning_cnh.alert.alert-warning").fadeIn();
				$(".msg_warnning_cnh.alert.alert-warning").html(msgNotExistis);
				$(".result_cnh").hide();
				$(".error.alert.alert-danger").hide();
				return;
			}

			if (data == msgToken) {
				$(".msg_warnning_cnh.alert.alert-warning").fadeIn();
				$(".msg_warnning_cnh.alert.alert-warning").html(msgToken);
				$(".result_cnh").hide();
				$(".error_cnh.alert.alert-danger").hide();
				return false;
			}

			$(".msg_warnning_cnh.alert.alert-warning").hide();

			// FOTO CNH
			let biometria_face = data["biometria_face"];
			let disp = biometria_face["disponivel"];
			if (disp == "false") {
				disp = "Não";
			} else {
				disp = "Sim";
			}
			let probabilidade = biometria_face["probabilidade"];
			let similaridade = biometria_face["similaridade"];
			// let disponivel = biometria_face["disponivel"];

			let image = document.getElementById("preview").src
			let getimage = $('.image-cnh').children('img').attr('src', image)[0]["currentSrc"];

			$(".generatepdfcnh").html('<a href="/transporter/drivers/details-cnh/'+probabilidade+'/'+disp+'/'+similaridade+'/'+image_biometer+'" class="download-pdf-search">Baixar PDF</a>');

			axios({
				method: "get",
				url: "/transporter/drivers/search/cnh/"+image_biometer+"/"+probabilidade+"/"+disp+"/"+similaridade,
				data: {},
				
			})
			.then(function(response) {})

			if (disp == false) {

				$(".result_cnh").hide();
				$(".msg_warnning_cnh.alert.alert-warning").fadeIn();
				$(".msg_warnning_cnh.alert.alert-warning").html("Nenhum dado retornado, verifique os dados inseridos e tente novamente!");
				return;

			}

			$(".biometria_probabilidade span p").html(probabilidade);
			if (similaridade == undefined) {

			} else {
				$(".similaridade span p").html(similaridade.toFixed(2));
			}
			$(".biometria_disp span p").html(disp);

			$(".result_cnh.mt-5").css({"display": "flex", "flex-wrap": "wrap"});

		})
		.catch(function(error) {
		});

});

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".input_cnh_cpf").change(function(){
    readURL(this);
});



$('.baixar-pdf-cnh').on("click", function(e){

	let prob = $(".biometria_probabilidade p").text();
	let disp = $(".biometria_disp p").text();
	let sim = $(".similaridade p").text();

	console.log(prob)

	axios({
		method: "get",
		url: "/transporter/drivers/details-cnh/"+"prob"+"/"+disp+"/"+sim
	})
	.then(function(response) {
		console.log(response);
	})

});

$(".error_cnh.alert.alert-danger").hide();
$(".msg_warnning_cnh.alert.alert-warning").hide();

$(".animate_travel").hide();

$(".error_travel.alert.alert-danger").hide();
$(".baixar-pdf").hide();
$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").hide();

$(".form_search_info_travels").submit(function(e) {
	$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").hide();
	e.preventDefault();

	$(".animate_travel").show();

	$(".table.mt-5").remove();

	let cpf = $("#searchtravel").val();
	let msgErro = "CPF digitado não encontrado, verifique e tente novamente.";

	if (!cpf || cpf == '') {
		$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").hide();
		$(".animate_travel").hide();
		$(".error_travel.alert.alert-danger").fadeIn();
		$(".error_travel.alert.alert-danger").html(msgErro);
		$(".result_travel").hide();
		return;
	}

	axios.post('/transporter/drivers/search/travel', JSON.stringify({
			cpf: cpf
		}))
		.then(function(response) {

			$(".animate_travel").hide();

			let msgNotExistis = "Não foram encontrados dados desse CPF!";

			if (response["data"] == "CPF digitado não encontrado, verifique e tente novamente.") {

				$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").show();
				$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").html(msgNotExistis);
				$(".result_travel").hide();
				$(".error_travel.alert.alert-danger").hide();
				return;
			}

			$(".msg_not_exists_travel.alert.alert-warning.mt-5.mb-5").hide();

			$(".baixar-pdf").show();

			$(".baixar-pdf").html('<a class="download-pdf-search" href="/transporter/drivers/details/' + cpf + '">Baixar Pdf</a>');

			let data = JSON.parse(JSON.stringify(response["data"]));

			templateString = '<table class="table mt-5"><tr><th>Nome</th><th>Celular</th><th>CPF</th><th>Data de Carregamento</th><th>Carga</th><th>Transportadora</th><th>Placa do Cavalo</th><th>Valor</th></tr>'
			$.each(data, function(i) {

				templateString += '<tr><td>' + data[i].namedriver + '</td><td>' + data[i].nrphone + '</td><td>' + data[i].cpfdriver + '</td><td>' + data[i].datereference + '</td><td>' + data[i].typecargo + '</td><td>' + data[i].nametransporter + '</td><td>' + data[i].board + '</td><td>' + data[i].pricecargo + '</td></tr>'
			})
			templateString += '</table>'
			$('.result_travel').append(templateString);
			return false;

		}).catch(function(error) {
		});

	axios.get('/transporters/savepdf/travel/' + cpf, JSON.stringify({
			cpf: cpf
		}))
		.then(function(response) {
		})

	return false;
});

$(".input-group-cnh-select").click(function() {

	if ($(".input-group-cnh-select :selected").val() == 'search-complete') {

		$(".form-input-image-cnh").fadeIn();
		$(".form-input-image-cnh-formtwo").fadeOut();

	} else if ($(".input-group-cnh-select :selected").val() == 'search-basic') {

		$(".form-input-image-cnh-formtwo").fadeIn();
		$(".form-input-image-cnh").fadeOut();

	}

});

let bandeira = $('.img-bandeira').text();

if (bandeira == 'mastercard') {
	$('.img-bandeira').html('<i class="fab fa-cc-mastercard"></i>');
} else if (bandeira == 'visa') {
	$('.img-bandeira').html('<i class="fab fa-cc-visa"></i>');
} else if (bandeira == 'amex') {
	$('.img-bandeira').html('<i class="fab fa-cc-amex"></i>');
} else if (bandeira == 'boleto') {
	$('.img-bandeira').html('<i class="fas fa-barcode"></i>');
}

$('.filters_button').click(function() {
	$('.slide_filters').css({
		'right': '0',
		'transition': '.5s'
	});
})

$('.close_filters').click(function() {
	$('.slide_filters').css({
		'right': '-100%',
		'transition': '.5s'
	});
})

$('.send_filter').click(function() {
	$('.slide_filters').css({
		'right': '-100%',
		'transition': '.5s'
	});
})

// $('.main-content').click(function(){
// $('.slide_filters').css({'right': '-100%','transition':'.5s'});
// })

$(".modal_csv").click(function() {
	$(".container-modal, .bg-modal").fadeIn();
});

$(".close_modal, .modal-example").click(function() {
	$(".container-modal, .bg-modal").fadeOut();
});

$(".content-modal-example").click(function() {
	return false;
});

$('.btn-download-csv').click(function() {
	window.location.href = '/views/assets/file/modelo_csv.xlsx'
})

$(".cep_driver, .result_serasa .cep, .cep_transporter").mask("99.999-999");

$(".phone_driver, .celaccountdriver").mask("(99) 99999 - 9999");
$(".birthdate_driver, .formGroupExampleInputdata, .birthdateaccountdriver").mask("99/99/9999");
$(".mask-date").mask("99/99");

$(".cpf_driver_edit, .show_cpf_driver, .cpf, #searchserasa, .formGroupExampleInputCpfdpf, .cpfaccountdriver").mask(
	"999.999.999-99");
$(".cep_driver_edit, .cepaccountdriver").mask("99.999-999");
$(".birthdate_driver_edit, #formGroupExampleInputdata").mask("99/99/9999");
$(".nrphone_driver_edit, .show_nrphone_driver, .phone_transporter").mask("(99) 99999-9999");

var options = {
	onKeyPress: function(cpf, ev, el, op) {
		var masks = ['000.000.000-000', '00.000.000/0000-00'];
		$('.cpf_driver').mask((cpf.length > 14) ? masks[1] : masks[0], op);
	}
}

$('.cpf_driver').length > 11 ? $('.cpf_driver').mask('00.000.000/0000-00', options) : $('.cpf_driver').mask(
	'000.000.000-00#', options);

$('.buttons_image').click(function() {
	$('.content_profile_transporter').toggle();
});

$("#idDoImput").click(function() {
	$("#idDoImput").val('');
});

$(".upload_form_csv").submit(function(e) {
	e.preventDefault();

	var form = $(this);
	var div_success = $(".progress");
	var div_error = $("alert-danger");
	var send_success = $(".alert-danger-success");
	var send_fail = $(".alert-danger-error");

	$.ajax({
		xhr: function() {
			var xhr = new window.XMLHttpRequest();

			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {

					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);

					div_success.css("display", "flex");

					$(".progress-bar").html(percentComplete + '%');
					$(".progress-bar").width(percentComplete + '%');

				}
			}, false);

			return xhr;

		},
		type: 'post',
		url: '/transporter/drivers/import',
		data: new FormData(this),
		contentType: false,

		cache: false,
		processData: false,
		success: function(data) {

			let result = data;
			$(".message-alert").html(
				'<div class="alert alert-danger-success"><i class="ri-error-warning-fill error-msg"></i>' +
				data + '</div>');

		},
		error: function(data) {},
		complete: function(data) {

		},
	});

});

$("#idDoImput").click(function() {
	$("#idDoImput").val('');
});

$(".form-input-image").submit(function(e) {
	e.preventDefault();

	var form = $(this);
	var div_success = $(".progress");
	var div_error = $("alert-danger");
	var send_success = $(".alert-danger-success");
	var send_fail = $(".alert-danger-error");

	$.ajax({
		xhr: function() {
			var xhr = new window.XMLHttpRequest();

			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {

					var percentComplete = evt.loaded / evt.total;
					percentComplete = parseInt(percentComplete * 100);

					div_success.css("display", "flex");

					$(".progress-bar").html(percentComplete + '%');
					$(".progress-bar").width(percentComplete + '%');

				}
			}, false);

			return xhr;

		},
		type: 'post',
		url: '/transporter/drivers/upload-profile',
		data: new FormData(this),
		contentType: false,

		cache: false,
		processData: false,
		success: function(data) {
			let result = data;
			$(".message-alert").html(
				'<div class="alert alert-danger-success"><i class="ri-error-warning-fill error-msg"></i>' +
				data + '</div>');

			window.location = "/transporter/account";

		},
		error: function(data) {

			(".message-alert-error").html(
				'<div class="alert alert-danger-error"><i class="ri-error-warning-fill error-msg"></i>' +
				data + '</div>');

		},
		complete: function(data) {

		},
	});

});

function limpa_formulário_cep() {
	// Limpa valores do formulário de cep.
	$(".cep_driver").val("");
	$(".neighborhood_driver").val("");
	$(".city_driver").val("");
	$(".street_driver").val("");

}

//Quando o campo cep perde o foco.
$(".cep_driver").blur(function() {

	//Nova variável "cep" somente com dígitos.
	var cep = $(this).val().replace(/\D/g, '');

	//Verifica se campo cep possui valor informado.
	if (cep != "") {

		//Expressão regular para validar o CEP.
		var validacep = /^[0-9]{8}$/;

		//Valida o formato do CEP.
		if (validacep.test(cep)) {

			//Preenche os campos com "..." enquanto consulta webservice.
			$(".street_driver").val("...");
			$(".neighborhood_driver").val("...");
			$(".city_driver").val("...");

			//Consulta o webservice viacep.com.br/
			$.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

				if (!("erro" in dados)) {
					//Atualiza os campos com os valores da consulta.
					$(".street_driver").val(dados.logradouro);
					$(".neighborhood_driver").val(dados.bairro);
					$(".city_driver").val(dados.localidade);

				} //end if.
				else {
					//CEP pesquisado não foi encontrado.
					limpa_formulário_cep();
					//    alert("CEP não encontrado.");
				}
			});
		} //end if.
		else {
			//cep é inválido.
			limpa_formulário_cep();
			//  alert("Formato de CEP inválido.");
		}
	} //end if.
	else {
		//cep sem valor, limpa formulário.
		limpa_formulário_cep();
	}
});

$(".icon_notification").on("click", function() {
	$(".container_notifcations").slideToggle();
});

$(".error_dpf.alert.alert-danger").hide();
$(".msg_warnning_dpf.alert.alert-warning").hide();
$(".result_dpf").hide();
$(".load_result").hide();
$(".error_dpf").hide();

$(".update-message-read").submit(function(e) {
	e.preventDefault();

	if ($('.notifications_content span').html() > 0) {

		axios({
				method: "post",
				url: "/transporter/verify-message",

			})
			.then(function(response) {

			})

	}
	$(".notifications_content span").hide();

});

var cpf = $("#searchserasa").val();

$(".generatepdfserasa").html('<a href="/transporter/drivers/details-serasa/' +
	cpf +
	'" class="download-pdf-search">Baixar PDF</a>');

$(".animate_dpf").hide();
$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").hide();

$(".search-dpf").submit(function(e) {

	$(".result_dpf").hide();
	$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").hide();

	$(".certidao_codigo div").html("");
	$(".data_emissao div").html("");
	$(".mensagem").html("");
	$(".site div").html("");
	$(".validade div").html("");

	e.preventDefault();

	var name = $(".formGroupExampleInputNamedpf").val();
	var cpf = $(".formGroupExampleInputCpfdpf").val();
	var data = $(".formGroupExampleInputdata").val();

	let msgErrorFields = "Preencha todos os campos para continuar!";

	if (!name || name == '' && !cpf || cpf == '' && !data || data == '') {
		$(".error_dpf.alert.alert-danger").fadeIn();
		$(".error_dpf.alert.alert-danger").html(msgErrorFields);
		$(".msg_warnning_dpf.alert.alert-warning").hide();
		$(".result_dpf").hide();
		return;
	}

	$(".animate_dpf").show();

	axios({
			method: "post",
			url: "/transporter/drivers/search/dpf",
			data: {
				name: name,
				cpf: cpf,
				data: data
			},
		})
		.then(function(response) {

			$(".error_dpf.alert.alert-danger").hide();

			$(".animate_dpf").hide();

			let msgToken = "Seus tokens para pesquisa expiraram!";
			let msgError = "Aconteceu um erro verifique os campos ou tente novamente mais tarde!";

			if (response["data"] == msgToken) {
				$(".animate_dpf").hide();
				$(".msg_warnning_dpf.alert.alert-warning").fadeIn();
				$(".msg_warnning_dpf.alert.alert-warning").html(response["data"]);
				$(".result_dpf").hide();
				$(".animate_dpf").hide();
				$(".error_dpf.alert.alert-danger").hide();
				return
			}

			let msgNotExistis = "Não foram encontrados dados, verifique os dados inseridos!";

			if (response["data"] == "" || response == null || response["data"] ==
				"Inconsistência nos dados com o CPF informado.") {
				$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").fadeIn();
				$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").html(msgNotExistis);
				$(".result_dpf").hide();

				$(".error.alert.alert-danger").hide();
				return;
			}

			$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").hide();
			$(".error_dpf.alert.alert-danger").hide();

			$(".animate_dpf").hide();

			$(".result_dpf").show();

			let itemObj = response["data"][0];
			let certidao_codigo = itemObj["certidao_codigo"];
			let data_emissao = itemObj["emissao_datahora"];
			let mensagem = itemObj["mensagem"];
			let site = itemObj["site_receipt"];
			let validade = itemObj["validade_data"];

			$(".button-dpf-pdf").html('<a class="download-pdf-search" href="'+site+'" target="_blank">Baixar Pdf</a>');

			if (response == '"Aconteceu um erro verifique os campos ou tente novamente mais tarde!"') {

				let cmsg = response.replace(/[\\"]/g, '');

				$(".result_dpf").hide();
				$(".error p").html(cmsg);

				$(".error p").show();

			} else {
				$(".error p").hide();
			}

			$(".certidao_codigo div").html(certidao_codigo);
			$(".data_emissao div").html(data_emissao);
			$(".mensagem").html(mensagem);
			$(".site div").html("<a class='btn-info p-2' href='" + site + "'>Baixar documento</a>");
			$(".validade div").html(validade);

			let cpfFormat = cpf.replace('.', '').replace('.', '').replace('-', '');
			let dateFormat = data.replace('/', '-').replace('/', '-').replace('/', '-');

			$(".result_dpf").show();

			axios({
					method: "post",
					url: "/transporters/savepdf/dpf",
					data: {
						cpf: cpf,
						certidao_codigo: certidao_codigo,
						data_emissao: data_emissao,
						mensagem: mensagem,
						validade: validade
					},
				})
				.then(function(response) {

				}).catch(function(response) {})
		})
		.catch(function(response) {

		})

	return false;

});

$(".error_serasa.alert.alert-danger.mt-5.mb-5, .msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").hide();
$(".animate_serasa").hide();
$(".result_serasa").hide();
$(".form_search_info_serasa").submit(function(e) {

	// $(".result_dpf").hide();
	$(".col-12.mt-5.msg_warnning_dpf.alert.alert-warning").hide();

	$(".result_serasa").hide();

	$(".tableconsulting table, .similar-names table, .similar-names span, .participacaosocietaria table, .participacaosocietaria span, .result-segments tr")
		.remove();
	$(".tabledatatypeone").remove();
	$(".phone-alternativi-two").remove();
	$(".phone-alternativi > span").remove();

	$(".pedency-tr .pendency-qtd, .pendency-period, .pendency-amount, .pendency-recent, .refin-qtd, .refin-period, .refin-amount, .refin-recent, .cheque-qtd, .cheque-period, .cheque-amount, .cheque-recent, .protest-qtd, .protest-period, .protest-amount, .protest-recent, .actions-qtd, .actions-period, .actions-amount, .actions-recent, .result-scoring, .chance-pagamento, .protestscatory, .protestscity, .protestsuf, .protestsdate, .price-protests, .tablecomplements tbody td, .name, .cpf, .birthdate, .nameMother, .street, .neighborhood, .city, .uf, .cep, .date, .statusdocument, .phonehome, .phonecom, .ramal, .celphone, .gender, .situationcpf .updatecpf .date, .street, .neighborhood, .city, .uf, .cep, .phonehome, .phonecom, .ramal, .celphone, .nrphone-alternativo, .streetalternativo, .neighborhood-alternativo, .cep-alternativo, .citytypetwo, .dt-update-two, .namecpftwo, .uftypetwo, .consulting-serasa-credit, .consulting-serasa-cheque, .result-scoring, .chance-pagamento, .dateconsulting, .origin, .result-scoring, .chance-pagamento, .result-scoring, .chance-pagamento, .protestscatory, .protestscity, .protestsuf, .protestsdate, .format-price, .protests-resume, .tabledatatypeone, .tabledatatypetwo, .tableconsulting, .tableprotests, .certidao_codigo div, .data_emissao div, .mensagem, .site div, .validade div, .estimatedincome")
		.html("");

	var cpf = $("#searchserasa").val();

	$(".generatepdfserasa").html('<a href="/transporter/drivers/details-serasa/' +
		cpf +
		'" class="download-pdf-search">Baixar PDF</a>');

	e.preventDefault();

	let msgErrorFields = "Preencha todos os campos para continuar!";

	if (!cpf || cpf == '') {
		$(".error_serasa.alert.alert-danger").fadeIn();
		$(".error_serasa.alert.alert-danger").html(msgErrorFields);
		$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").hide();
		$(".result_serasa").hide();
		return;
	}

	$(".animate_serasa").show();

	axios({
			method: "post",
			url: "/transporter/drivers/search/serasa",
			data: {
				cpf: cpf
			},
		})
		.then(function(response) {

			$(".animate_serasa").hide();

			// $(".error_dpf").html(response);

			let msgToken = "Seus tokens para pesquisa expiraram!";
			let msgError = "Aconteceu um erro verifique os campos ou tente novamente mais tarde!";

			if (response["data"] == msgToken) {
				$(".animate_serasa").hide();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").fadeIn();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").html(response["data"]);
				$(".result_serasa").hide();
				$(".animate_serasa").hide();
				$(".error_serasa.alert.alert-danger").hide();
				return
			}

			if (response["data"] == "") {
				$(".animate_serasa").hide();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").fadeIn();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").html(
					"O CPF digitado não existe, verifique e digite um CPF válido para continuar!");
				$(".result_serasa").hide();
				$(".animate_serasa").hide();
				$(".error_serasa.alert.alert-danger").hide();
			}

			if (response["data"] == "CPF Inválido") {
				$(".animate_serasa").hide();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").fadeIn();
				$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").html(
					"CPF inválido, digite um CPF válido para continuar!");
				$(".result_serasa").hide();
				$(".animate_serasa").hide();
				$(".error_serasa.alert.alert-danger").hide();
				return
			}

			let msgNotExistis = "Não foram encontrados dados, verifique os dados inseridos!";

			if (response["data"] == "" || response == null || response["data"] ==
				"Inconsistência nos dados com o CPF informado.") {
				$(".col-12.mt-5.msg_warnning_serasa.alert.alert-warning").fadeIn();
				$(".col-12.mt-5.msg_warnning_serasa.alert.alert-warning").html(msgNotExistis);
				$(".result_serasa").hide();

				$(".error_serasa.alert.alert-danger").hide();
				return;
			}

			$(".col-12.mt-5.msg_warnning_serasa.alert.alert-warning").hide();
			$(".msg_not_exists_serasa.alert.alert-warning.mt-5.mb-5").hide();
			$(".error_serasa.alert.alert-danger").hide();

			$(".animate_serasa").hide();

			$(".result_serasa .cep").mask("99.999-999");

			let data = response["data"].dataDriver;

			if (data[0] !== null || data[0] !== "" || data[1] !== null || data[1] == "") {
				// identificacion
				$(".result_serasa").show();
				$(".name").html(
					data[0]["B001"]["identificacao"]["namedriver"] !== '' ?
					data[0]["B001"]["identificacao"]["namedriver"] :
					'-');
				$(".cpf_driver").html(
					data[0]["B001"]["identificacao"]["cpfdriver"] !== '' ?
					data[0]["B001"]["identificacao"]["cpfdriver"] :
					'-');
				$(".birthdate").html(
					data[0]["B001"]["identificacao"]["birthdatedriver"] !== '' ?
					data[0]["B001"]["identificacao"]["birthdatedriver"] :
					data[0]["B001"]["identificacao"]["birthdatedriver"] == '' ? '-' : '-');
				if (data[1] == null || data[1] == "" || data[1] == 'undefined') {
					$(".nameMother").html('-');
				} else {
					$(".nameMother").html(
						data[1]["B002"]["cadsintetic"]["namemother"] !== '' ?
						data[1]["B002"]["cadsintetic"]["namemother"] :
						data[1]["B002"] == '' ? '-' : '-'
					);
				}
			} else {
				$(".result_serasa").show();
				$(".name").html('-');
				$(".cpf").html('');
				$(".birthdate").html('-');
				$(".nameMother").html('-');
			}

			if (data[1] == null || data[1] == "" || data[1] == 'undefined') {
				$(".gender").html("-");
			} else {

				if (data[1]["B002"]["cadsintetic"]["gender"] == "M") {
					$(".gender").html(data[1]["B002"]["cadsintetic"]["gender"] !== '' ?
						"Masculino" :
						data[1]["B002"]["cadsintetic"]["gender"] == '' ? '-' : '-');
				} else {
					$(".gender").html(data[1]["B002"]["cadsintetic"]["gender"] !== '' ?
						"Feminino" :
						data[1]["B002"]["cadsintetic"]["gender"] == '' ? '-' : '-');
				}

			}

			//similar name

			if (data[17] == null || data[17] == "") {
				templateString =
					'<span>Nomes Similares</span>' +
					'<table class="record-serasa">' +
					'<thead><tr><th>Nome</th></tr></thead>';

				templateString +=
					'<tr><td class="similarname">' +
					'-' +
					'</td></tr>'

				templateString += '</table>';

				$('.similar-names').append(templateString);
			} else {

				let alternativo = JSON.parse(JSON.stringify(data[17]["similarnames"][0]));

				templateString =
					'<span>Nomes Similares</span><table class="record-serasa"><thead><tr><th class="col-5">Nome</th></thead>';

				$.each(alternativo, function(i) {
					templateString +=
						'<tr><td class="similarname col-5">' +
						alternativo[i].similarnames +
						'</td></tr>'

				});

				templateString += '</table>';

				$('.similar-names').append(templateString);
			}

			if (data[0] == null || data[0] == "") {

				$(".statusdocument").html(
					data[0]["B001"]["identificacao"]["situationcpf"] !== '' ?
					data[0]["B001"]["identificacao"]["situationcpf"] == '' :
					data[0]["B001"]["identificacao"]["situationcpf"] == '' ? '-' : "-");

			} else {

				$(".date").html(data[0]["B001"]["identificacao"]["updatecpf"]);

				if (data[0]["B001"]["identificacao"]["situationcpf"] == 2) {
					$(".statusdocument").html("Regular");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 3) {
					$(".statusdocument").html("Pendente de Regularização");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 6) {
					$(".statusdocument").html("Suspensa");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 7) {
					$(".statusdocument").html("Titular falecido");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 4) {
					$(".statusdocument").html("Nulo");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 9) {
					$(".statusdocument").html("Cancelado");
				} else if (data[0]["B001"]["identificacao"]["situationcpf"] == 0) {
					$(".statusdocument").html("CPF não confirmado até esta data");
				}
			}

			// street
			if (data[3] == null || data[3] == "") {

				$(".street").html('-');
				$(".neighborhood").html('-');
				$(".city").html('-');
				$(".uf").html('-');
				$(".cep").html('-');

			} else {

				// data[0] !== '' ? data[0]["B001"]["identificacao"]["namedriver"] : data[1] == '' ? '-' : '-'
				$(".street").html(
					data[3]["B004"]["street"]["street"] !== '' ?
					data[3]["B004"]["street"]["street"] :
					data[3]["B004"]["street"]["street"] == '' ? '-' : '-');
				$(".neighborhood").html(
					data[3]["B004"]["street"]["neighborhood"] !== '' ?
					data[3]["B004"]["street"]["neighborhood"] :
					data[3]["B004"]["street"]["neighborhood"].trim() == '' ? '-' : '-');
				$(".city").html(
					data[3]["B004"]["street"]["city"] !== '' ?
					data[3]["B004"]["street"]["city"] :
					data[3]["B004"]["street"]["city"] == '' ? '-' : '-');
				$(".uf").html(
					data[3]["B004"]["street"]["uf"] !== '' ?
					data[3]["B004"]["street"]["uf"] :
					data[3]["B004"]["street"]["uf"] == '' ? '-' : '-');
				$(".cep").html(data[3]["B004"]["street"]["cep"] !== '' ?
					data[3]["B004"]["street"]["cep"] :
					data[3]["B004"]["street"]["cep"] == '' ? '-' : '-');
			}

			if (data[2] == null || data[2] == "") {
				$(".phonehome").html('-');
				$(".phonecom").html('-');
				$(".ramal").html('-');
				$(".celphone").html('-');

			} else {
				// $(".name").html(data[0] !== '' ? data[0]["B001"]["identificacao"]["namedriver"] : data[1] == '' ? '-' : '-');
				$(".phonehome").html(
					data[2]["B003"]["cadsinteticphone"]["dddphone"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["dddphone"] +
					data[2]["B003"]["cadsinteticphone"]["phonehome"] :
					data[2]["B003"]["cadsinteticphone"]["phonehome"] == '' ? '-' : '-');
				$(".phonecom").html(
					data[2]["B003"]["cadsinteticphone"]["dddphonecom"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["dddphonecom"] +
					data[2]["B003"]["cadsinteticphone"]["phonecom"] :
					data[2]["B003"]["cadsinteticphone"]["phonecom"] == '' ? '-' : '-');
				$(".phonehome, .phonecom").mask("(99)9999 - 9999");
				$(".ramal").html(data[2]["B003"]["cadsinteticphone"]["ramal"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["ramal"] :
					data[2]["B003"]["cadsinteticphone"]["ramal"] == '' ? '-' : '-');

				$(".celphone").html(
					data[2]["B003"]["cadsinteticphone"]["ramal"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["dddcell"] +
					data[2]["B003"]["cadsinteticphone"]["cell"] :
					data[2]["B003"]["cadsinteticphone"]["cell"] == '' ? '-' : '-');
				$(".celphone").mask("(99)99999 - 9999");
			}

			if (data[6] == null || data[6] == "") {

				templateString =
					'<span>Endereço e Contatos alternativos</span><table class="record-serasa tabledatatypeone"><thead><tr><th>Telefone</th><th>Endereço</th><th>Bairro</th><th>CEP</th></thead></tr>';

				templateString +=
					'<tr><td class="nrphone-alternativo">' +
					'-' +
					'</td><td class="streetalternativo">' +
					'-' +
					'</td><td class="neighborhood-alternativo">' +
					'-' +
					'</td><td class="cep-alternativo">' +
					'-' +
					'</td></tr>'

				templateString += '</table>';

				$('.phone-alternativi').append(templateString);

			} else {

				let alternativo = JSON.parse(JSON.stringify(data[6]["B3701"][0]));

				templateString =
					'<span>Endereço e Contatos alternativos</span><table class="record-serasa tabledatatypeone"><thead><tr><th>Telefone</th><th>Endereço</th><th>Bairro</th><th>CEP</th></thead></tr>';

				$.each(alternativo, function(i) {
					templateString +=
						'<tr><td class="nrphone-alternativo">' +
						alternativo[i].dddtypeone + alternativo[i].nrphonetypeone +
						'</td><td class="streetalternativo">' +
						alternativo[i].streettypeone +
						'</td><td class="neighborhood-alternativo">' +
						alternativo[i].neighborhoodtypeone +
						'</td><td class="cep-alternativo">' +
						alternativo[i].ceptypeone +
						'</td></tr>'

				});

				templateString += '</table>';

				$('.phone-alternativi').append(templateString);
				$('.cep-alternativo').mask("99.999-999");

				$(".nrphone-alternativo").mask("(99)9999-9999");

			}

			if (data[7] == null || data[7] == "") {

				templateStringTwo =
					'<span>Endereço alternativo</span><table class="record-serasa tabledatatypetwo"><thead><tr><th>Cidade</th><th>Data de Atualização</th><th>Nome do CPF</th><th>UF</th></thead></tr>';

				templateStringTwo +=
					'<tr><td class="citytypetwo">' +
					'-' +
					'</td><td class="dt-update-two">' +
					'-' +
					'</td><td class="namecpftwo">' +
					'-' +
					'</td><td class="uftypetwo">' +
					'-' +
					'</td></tr>'

				templateStringTwo += '</table>'
				$('.phone-alternativi-two').append(templateStringTwo);

			} else {

				let alternativoTwo = JSON.parse(JSON.stringify(data[7]["B3702"][0]));

				templateStringTwo =
					'<span>Endereço alternativo</span><table class="record-serasa tabledatatypetwo"><thead><tr><th>Cidade</th><th>Data de Atualização</th><th>Nome do CPF</th><th>UF</th></thead></tr>';

				$.each(alternativoTwo, function(i) {
					templateStringTwo +=
						'<tr><td class="citytypetwo">' +
						alternativoTwo[i].citytypetwo +
						'</td><td class="dt-update-two">' +
						alternativoTwo[i].dtupdatetypetwo +
						'</td><td class="namecpftwo">' +
						alternativoTwo[i].namecpftypetwo +
						'</td><td class="uftypetwo">' +
						alternativoTwo[i].uftypetwo +
						'</td></tr>'
				});
				templateStringTwo += '</table>'
				$('.phone-alternativi-two').append(templateStringTwo);

			}

			if (data[4] == null || data[4] == "") {

				$('.consulting-serasa-credit').html('-');
				$('.consulting-serasa-cheque').html('-');

			} else {

				$('.consulting-serasa-credit').html("<p>" + "Total de consultas crédito = " + data[4][
					"B353"
				][0][0]["qtdtotal"] + "(Ultimo mês: " + data[4]["B353"][0][0][
					"qtdtotallastmonth"
				] + ")" + "</p>");
				$('.consulting-serasa-cheque').html("<p>" + "Total de consultas cheque = " + data[4]["B353"]
					[0][0]["totalconsultingcheques"] + "(Ultimo mês: " + data[4]["B353"][0][0][
						"qtdtotallastmonthcheque"
					] + ")" + "</p>");

			}

			if (data[8] == null || data[8] == "") {
				$(".result-scoring").html('-');
				$(".chance-pagamento").html('-');
			} else {

				$(".result-scoring").html(data[8]["B280BPHM"][0][0]["score"] !== '' ? data[8]["B280BPHM"][0]
					[0]["score"].replace(/^0+/,'') : data[8]["B280BPHM"][0][0]["score"] == '' ? '-' : '-');
				$(".chance-pagamento").html(data[8]["B280BPHM"][0][0]["percentage"] !== '' ? data[8][
						"B280BPHM"
					][0][0]["percentage"] + "%" : data[8]["B280BPHM"][0][0]["percentage"] == '' ? '-' :
					'-');
			}

			if (data[9] == null || data[9] == "") {
				let segments = JSON.parse(JSON.stringify(data[9]["B354"][0]));

				templateStringTwoSegments = '<tr class=" d-flex flex-wrap w-100 tableconsulting">';
				$.each(segments, function(i) {
					templateStringTwoSegments +=
						'<td class="col-6 dateconsulting">' +
						'-' +
						'</td><td class="col-6 origin">' +
						'-' +
						'</td>'

				});
				templateStringTwoSegments += '</tr>'

				$('.result-segments').append(templateStringTwoSegments);
			} else {
				let segments = JSON.parse(JSON.stringify(data[9]["B354"][0]));

				templateStringTwoSegments = '<tr class=" d-flex flex-wrap w-100 tableconsulting">';
				$.each(segments, function(i) {
					templateStringTwoSegments +=
						'<td class="col-6 dateconsulting">' +
						segments[i].dateconsulting +
						'</td><td class="col-6 origin">' +
						segments[i].origin +
						'</td>'

				});
				templateStringTwoSegments += '</tr>'

				$('.result-segments').append(templateStringTwoSegments);
			}

			if (data[8] == null || data[8] == "") {

				$(".result-scoring").html('-');
				$(".chance-pagamento").html('-');

			} else {
				

				$(".result-scoring").html(
					// '<div id="chartDiv" style="max-width: 740px;height: 330px;margin: 0px auto"></div>'

					data[8]["B280BPHM"][0][0]["score"] !== '' ? data[8]["B280BPHM"][0]
					[0]["score"].replace(/^0+/,'') : data[8]["B280BPHM"][0][0]["score"] == '' ? '-' : '-');
				$(".chance-pagamento").html(data[8]["B280BPHM"][0][0]["percentage"] !== '' ? data[8][
						"B280BPHM"
					][0][0]["percentage"] + "%" : data[8]["B280BPHM"][0][0]["percentage"] == '' ? '-' :
					'-' 
					);

			}



			if (data[5] == null || data[5] == "") {

				templateprotests =
					'<span>Protestos</span><table class="record-serasa tableprotests"><thead><tr><th>Cartório</th><th>Cidade</th><th>UF</th><th>Data</th><th>Valor (R$)</th></thead></tr>';

				templateprotests +=
					'<tr><td class="protestscatory">' +
					'-' +
					'</td><td class="protestscity">' +
					'-' +
					'</td><td class="protestsuf">' +
					'-' +
					'</td><td class="protestsdate">' +
					'-' +
					'</td><td class="price-protests">' +
					'<div class="format-price">' + "-" + '</div>' +
					'</td>'

				templateprotests += '</table>'
				$('.protests-resume').append(templateprotests);

			} else {
				let protests = JSON.parse(JSON.stringify(data[5]["B362"][0]));

				templateprotests =
					'<span>Protestos</span><table class="record-serasa tableprotests"><thead><tr><th>Cartório</th><th>Cidade</th><th>UF</th><th>Data</th><th>Valor (R$)</th></thead></tr>';

				$.each(protests, function(i) {
					templateprotests +=
						'<tr><td class="protestscatory">' +
						protests[i].protestscatory.replace(/^0+/,'') +
						'</td><td class="protestscity">' +
						protests[i].protestscity +
						'</td><td class="protestsuf">' +
						protests[i].protestsuf +
						'</td><td class="protestsdate">' +
						protests[i].protestsdate +
						'</td><td class="price-protests">' +
						'<div class="format-price">"' + protests[i].protestsvalue.replace(/^0+/,
							'') + '00' + '"</div>' +
						'</td>'

				});

				templateprotests += '</table>'
				$('.protests-resume').append(templateprotests);

				$('.format-price').priceFormat({
					prefix: 'R$ ',
					centsSeparator: ',',
					thousandsSeparator: '.',

				});
			}

			if (data[11] == "" || data[11] == null) {
				$(".refin-qtd").html('-');
				$(".refin-period").html('-');
				$(".refin-amount").html('-');
				$(".refin-recent").html('-');
			} else {

				if (data[11]["pefin"][0]["type"] == "04") {

					$(".refin-qtd").html(data[11]["pefin"][0]["qtdocurrency"].replace(/^0+/,
						''));
					$(".refin-period").html(data[11]["pefin"][0]["dateminus"] + " a " + data[11]["pefin"][0]
						["datemax"]);
					$(".refin-amount").html(data[11]["pefin"][0]["valuetotal"]);
					$('.refin-amount').priceFormat({
						prefix: 'R$ ',
						centsSeparator: ',',
						thousandsSeparator: '.'
					});
					$(".refin-recent").html(data[11]["pefin"][0]["lastbusiness"]);

				} else if (data[11]["pefin"][1]["type"] == "04") {

					$(".refin-qtd").html(data[11]["pefin"][1]["qtdocurrency"]);
					$(".refin-period").html(data[11]["pefin"][1]["dateminus"] + " a " + data[11]["pefin"][1]
						["datemax"]);
					$(".refin-amount").html(data[11]["pefin"][1]["valuetotal"]);
					$('.refin-amount').priceFormat({
						prefix: 'R$ ',
						centsSeparator: ',',
						thousandsSeparator: '.'
					});
					$(".refin-recent").html(data[11]["pefin"][1]["lastbusiness"]);

				}
			}

			if (data[12] == "" || data[12] == null) {
				$(".pendency-qtd").html('-');
				$(".pendency-period").html('-');
				$(".pendency-amount").html('-');
				$(".pendency-recent").html('-');
			} else {

				if (data[12]["pefinpendency"][0][0]["type"] == "01") {

					$(".pendency-qtd").html(data[12]["pefinpendency"][0][0]["qtdocurrency"].replace(/^0+/,
						''));
					$(".pendency-period").html(data[12]["pefinpendency"][0][0]["dateminus"] + " a " + data[
						12]["pefinpendency"][0][0]["datemax"]);
					$(".pendency-amount").html(data[12]["pefinpendency"][0][0]["valuetotal"].replace(/^0+/,
						''));
					$('.pendency-amount').priceFormat({
						prefix: 'R$ ',
						centsSeparator: ',',
						thousandsSeparator: '.'
					});

					$(".pendency-recent").html(data[12]["pefinpendency"][0][0]["lastbusiness"]);

				}
			}

			if (data[13] == null || data[13] == "") {
				$(".cheque-qtd").html('-');
				$(".cheque-period").html('-');
				$(".cheque-amount").html('-');
				$(".cheque-recent").html('-');
			} else {

				$(".cheque-qtd").html(data[13]["B359"][0][0]["qtdocurrency"].replace(/^0+/,
					''));
				$(".cheque-period").html(data[13]["B359"][0][0]["dateminus"] + " a " + data[13]["B359"][0][
					0
				]["datemax"]);
				$(".cheque-amount").html(data[13]["B359"][0][0]["valuetotal"]);
				$('.cheque-amount').priceFormat({
					prefix: 'R$ ',
					centsSeparator: ',',
					thousandsSeparator: '.'
				});
				$(".cheque-recent").html(data[13]["B359"][0][0]["lastbusiness"]);

			}

			if (data[10] == null || data[10] == "") {
				$(".protest-qtd").html('-');
				$(".protest-period").html('-');
				$(".protest-amount").html('-');
				$(".protest-recent").html('-');
			} else {

				$(".protest-qtd").html(data[10]["B361"][0][0]["totalocorrencie"].replace(/^0+/,
					''));
				let dateminus = $(".protest-period").html(data[10]["B361"][0][0]["dateminus"].replace(/(\d*)-(\d*)-(\d*).*/, '$3-$2-$1') + " a " + data[10]["B361"][0][0]["datelarger"].replace(/(\d*)-(\d*)-(\d*).*/, '$3-$2-$1'));
				

				

				$(".protest-amount").html(data[10]["B361"][0][0]["price"]);
				$('.protest-amount').priceFormat({
					prefix: 'R$ ',
					centsSeparator: ',',
					thousandsSeparator: '.'
				});
				$(".protest-recent").html(data[10]["B361"][0][0]["city"]);

			}

			if (data[14] == null || data[14] == "") {
				$(".actions-qtd").html('-');
				$(".actions-period").html('-');
				$(".actions-amount").html('-');
				$(".actions-recent").html('-');

			} else {

				$(".actions-qtd").html(data[14]["B363"][0][0]["qtdocurrency"].replace(/^0+/,
					''));
				$(".actions-period").html(data[14]["B363"][0][0]["dateminus"] + " a " + data[14]["B363"][0][
					0
				]["datemax"]);
				$(".actions-amount").html(data[14]["B363"][0][0]["valuetotal"]);
				$('.actions-amount').priceFormat({
					prefix: 'R$ ',
					centsSeparator: ',',
					thousandsSeparator: '.'
				});
				$(".actions-recent").html(data[14]["B363"][0][0]["lastbusiness"]);

			}

			if (data[18] == null || data[18] == "") {

				templateparticipacao =
					'<span>Participacao Societaria</span><table class="record-serasa societaria"><thead><tr><th>Razão Social</th><th>CNPJ</th><th>Participação</th><th>Estado</th><th>Situação</th><th>Data de Inicio</th><th>Ultima Atualização</th></thead></tr>';

				templateparticipacao +=
					'<tr><td class="razaosocial">' +
					'-' +
					'</td><td class="cnpj">' +
					'-' +
					'</td><td class="participacao">' +
					'-' +
					'</td><td class="estado">' +
					'-' +
					'</td><td class="situacao">' +
					'-' +
					'</td><td class="dtinit">' +
					'-' +
					'</td><td class="lastupdate">' +
					'-' +
					'</td>'

				templateparticipacao += '</table>'
				$('.participacaosocietaria').append(templateparticipacao);

			} else {
				let participacao = JSON.parse(JSON.stringify(data[18]["participacaosocietaria"][0]));

				templateparticipacao =
					'<span>Participacao Societaria</span><table class="record-serasa societaria"><thead><tr><th>Razão Social</th><th>CNPJ</th><th>Participação</th><th>Estado</th><th>Situação</th><th>Data de Inicio</th><th>Ultima Atualização</th></thead></tr>';

				$.each(participacao, function(i) {
					templateparticipacao +=
						'<tr><td class="razaosocial">' +
						participacao[i].razaosocial +
						'</td><td class="cnpj">' +
						participacao[i].cnpj +
						'</td><td class="participacao">' +
						participacao[i].participacao.replace(/^0+/,'') +
						'</td><td class="estado">' +
						participacao[i].estado +
						'</td><td class="situacao">' +
						participacao[i].situacao +
						'</td><td class="dtinit">' +
						participacao[i].dtinit +
						'</td><td class="lastupdate">' +
						participacao[i].lastupdate +
						'</td>'

				});

				templateparticipacao += '</table>'
				$('.participacaosocietaria').append(templateparticipacao);

			}

			$(".complementname").html(
				data[0]["B001"]["identificacao"]["namedriver"] !== '' ?
				data[0]["B001"]["identificacao"]["namedriver"] :
				data[0]["B001"] == '' ? '-' : '-'
			);
			$(".complementcpf").html(
				data[0]["B001"]["identificacao"]["cpfdriver"] !== '' ?
				data[0]["B001"]["identificacao"]["cpfdriver"] :
				data[0]["B001"] == '' ? '-' : '-'
			);
			$(".complementrg").html(
				data[0]["B001"]["identificacao"]["idcard"] !== '' ?
				data[0]["B001"]["identificacao"]["idcard"] :
				data[0]["B001"] == '' ? '-' : '-'

			);

			if(data[1] == null || data[1] == ''){
				$(".complementorgao").html('-');
			} else {
				$(".complementorgao").html(
					data[1]["B002"]["cadsintetic"]["emissiondoc"] !== '' ?
					data[1]["B002"]["cadsintetic"]["emissiondoc"] : '-'
				);
			}

			if(data[1] == null || data[1] == ''){
				$(".complementuf").html('-');
			} else {
				$(".complementuf").html(
					data[1]["B002"]["cadsintetic"]["ufemissiondoc"] !== '' ?
					data[1]["B002"]["cadsintetic"]["ufemissiondoc"] : '-'
				);
			}
			
			if(data[1] == null || data[1] == ''){
				$(".complementdateemission").html('-');
			} else {
				$(".complementdateemission").html(
					data[1]["B002"]["cadsintetic"]["dateemissiondoc"] !== '' ?
					data[1]["B002"]["cadsintetic"]["dateemissiondoc"] : '-'
				);
			}
			

			$(".complementnascimento").html(

				data[0]["B001"]["identificacao"]["birthdatedriver"] !== '' ?
				data[0]["B001"]["identificacao"]["birthdatedriver"] :
				data[0]["B001"]["identificacao"]["birthdatedriver"] == '' ? '-' : '-'
			);

			if(data[2] == null || data[2] == ''){
				$(".complementufnasc").html('-');
			} else {
				$(".complementufnasc").html(
					data[2]["B003"]["cadsinteticphone"]["ufbirth"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["ufbirth"] : '-'
				);
			}

			if(data[2] == null || data[2] == ''){
				$(".complementestadocivil").html('-');
			} else {
				$(".complementestadocivil").html(
					data[2]["B003"]["cadsinteticphone"]["maritalstatus"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["maritalstatus"] : '-'
				);
			}

			if(data[2] == null || data[2] == ''){
				$(".complementdependentes").html('-');
			} else {
				$(".complementdependentes").html(
					data[2]["B003"]["cadsinteticphone"]["numdependencies"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["numdependencies"] : '-'
				);
			}

			if(data[2] == null || data[2] == ''){
				$(".complementescolaridade").html('-');
			} else {
				$(".complementescolaridade").html(
					data[2]["B003"]["cadsinteticphone"]["school"] !== '' ?
					data[2]["B003"]["cadsinteticphone"]["school"] : '-'
				);
			}
			

			if(data[19] == null || data[19] == ''){
				$(".estimatedincome").html('-');
			} else {
				$(".estimatedincome").html(
					data[19]["renda"][0][0]["renda"] !== '' ?
					data[19]["renda"][0][0]["renda"] : '-'
				);
			}

			$('.estimatedincome').priceFormat({
				prefix: 'R$ ',
				centsSeparator: ',',
				thousandsSeparator: '.'
			});

			

		//pdf

		let namedriver 	= $(".name").text();
		let cpfdriver 	= $(".cpf_driver").text();
		let birthdate 	= $(".birthdate").text();
		let nameMother 	= $(".nameMother").text();
		let gender 		= $(".gender").text();

		//status document                
		let documentdate 	= $(".date").text();
		let documentstatus 	= $(".statusdocument").text();

		//street              
		let street 			= $(".street").text();
		let neighborhood 	= $(".neighborhood").text();
		let city 			= $(".city").text();
		let uf 				= $(".uf").text();
		let cep 			= $(".cep").text();

		//Telefones              
		let phonehome 	= $(".phonehome").text();
		let phonecom 	= $(".phonecom").text();
		let ramal 		= $(".ramal").text();
		let celphone 	= $(".celphone").text();

		let consultingcreditserasa = $(".consulting-serasa-credit p").text();
		let consultingchequeserasa = $(".consulting-serasa-cheque p").text();


		let pendencyperiod 	= $(".pendency-period").text();		
		let pendencyqtd 	= $(".pendency-qtd").text();		

		let pendencyamount 	= $(".pendency-amount").text();		
		let pendencyrecent 	= $(".pendency-recent").text();		


		let refinperiod = $(".refin-period").text();		
		let refinqtd 	= $(".refin-qtd").text();		

		let refinamount 	= $(".refin-amount").text();		
		let refinrecent 	= $(".refin-recent").text();		
		let chequeperiod 	= $(".cheque-period").text();		
		let chequeqtd 		= $(".cheque-qtd").text();	
		let chequeamount 	= $(".cheque-amount").text();		
		let chequerecent 	= $(".cheque-recent").text();	
		let protestperiod 	= $(".protest-period").text();		
		let protestqtd 		= $(".protest-qtd").text();	
		let protestamount 	= $(".protest-amount").text();		
		let protestrecent 	= $(".protest-recent").text();	
		let actionsperiod 	= $(".actions-period").text();		
		let actionsqtd 		= $(".actions-qtd").text();	
		let actionsamount 	= $(".actions-amount").text();		
		let actionsrecent 	= $(".actions-recent").text();	
		let score 			= $(".result-scoring").text();		
		let chancepagamento = $(".chance-pagamento").text();	
		
		let complementcpf 			= $(".complementcpf").text();
		let complementname 			= $(".complementname").text();
		let complementorgao 		= $(".complementorgao").text();
		let complementrg 			= $(".complementrg").text();
		let complementdateemission 	= $(".complementdateemission").text();
		let complementuf 			= $(".complementuf").text();

		let complementnascimento 	= $(".complementnascimento").text();
		let complementufnasc 		= $(".complementufnasc").text();

		let complementdependentes 	= $(".complementdependentes").text();
		let complementestadocivil 	= $(".complementestadocivil").text();
		let estimatedincome 		= $(".estimatedincome").text();
		let complementescolaridade 	= $(".complementescolaridade").text();

		console.log("chegou aqui, antes do serasa 01");

		let similarname;
		if(data[17] == null || data[17] == ''){
			
		} else{
			similarname = JSON.parse(JSON.stringify(data[17]["similarnames"][0]));
		}

		console.log("chegou aqui, antes do serasa 02");

		let streetalternative
		if(data[6] == null || data[6] == ''){
			
		} else{
			streetalternative = JSON.parse(JSON.stringify(data[6]["B3701"][0]));
		}

		let updatecpf;
		if(data[0] == null || data[0] == ''){
			
		} else{
			updatecpf = $(".date").html(data[0]["B001"]["identificacao"]["updatecpf"]);
		}	
		
		let serasa;
		if(data[9] == null || data[9] == ''){
			
		} else{
			serasa = JSON.parse(JSON.stringify(data[9]["B354"][0]));
		}	
		
		let participacaosocietaria;
		if(data[18] == null || data[18] == ''){
			
		} else{
			participacaosocietaria = JSON.parse(JSON.stringify(data[18]["participacaosocietaria"][0]));
		}	
		
		let allprotests;
		if(data[5] == null || data[5] == ''){
			
		} else{
			allprotests = JSON.parse(JSON.stringify(data[5]["B362"][0]));
		}	

		console.log("chegou aqui, antes do serasa");

		axios({
			method: "post",
			url: "/transporters/savepdf/serasa",
			data: {
				cpf: cpf,
				namedriver: namedriver,
				cpfdriver: cpfdriver,
				birthdate: birthdate,
				nameMother: nameMother,
				gender: gender,
				documentdate: documentdate,
				documentstatus: documentstatus,
				street: street,
				neighborhood: neighborhood,
				city: city,
				uf: uf,
				cep: cep,
				similarname: similarname,
				phonehome: phonehome,
				phonecom: phonecom,
				ramal: ramal,
				celphone: celphone,
				streetalternative: streetalternative,
				serasa: serasa,
				consultingcreditserasa: consultingcreditserasa,
				consultingchequeserasa: consultingchequeserasa,
				pendencyqtd: pendencyqtd,
				pendencyperiod: pendencyperiod,
				pendencyamount: pendencyamount,
				pendencyrecent: pendencyrecent,
				refinqtd: refinqtd,
				refinperiod: refinperiod,
				refinamount: refinamount,
				refinrecent: refinrecent,
				chequeqtd: chequeqtd,
				chequeperiod: chequeperiod,
				chequeamount: chequeamount,
				chequerecent: chequerecent,
				protestqtd: protestqtd,
				protestperiod: protestperiod,
				protestamount: protestamount,
				protestrecent: protestrecent,
				actionsqtd: actionsqtd,
				actionsperiod: actionsperiod,
				actionsamount: actionsamount,
				actionsrecent: actionsrecent,
				score: score,
				chancepagamento: chancepagamento,
				allprotests: allprotests,
				participacaosocietaria: participacaosocietaria,
				complementname: complementname,
				complementcpf: complementcpf,
				complementrg: complementrg,
				complementorgao: complementorgao,
				complementuf: complementuf,
				complementdateemission: complementdateemission,
				complementnascimento: complementnascimento,
				complementufnasc: complementufnasc,
				complementestadocivil: complementestadocivil,
				complementdependentes: complementdependentes,
				complementescolaridade: complementescolaridade,
				estimatedincome: estimatedincome
			},
		}).then(function(response){console.log(response)})
		.catch(function(response){console.log(response)});
        console.log("chegou aqui, depois do serasa");
	})
	.catch(function(response) {})
	return false;

});

$(".ri-settings-3-line").click(function() {

	$(".menu-lateral-historicsearch").css({
		"right": "0",
		"transition": ".5s"
	});

});

$(".icon-close-historicsearch").click(function() {

$(".menu-lateral-historicsearch").css({
	"right": "-280px",
	"transition": ".5s"
});

});

$('.checkbox-menu').on('click', function() {

	var checkbox = $('.checkbox-menu:checked').length;

	var getClass = this.className.split('-');
	var last = getClass.pop();

	if ($(".menudriver-" + last).prop("checked") == true) {

		$(".menudriver-" + last).prop("checked", false)
		$(".menudriver-" + last).removeClass("openmenu");
	} else {
		$(".menudriver-" + last).prop("checked", true)
		$(".menudriver-" + last).addClass("openmenu");
	}

});

$(document).on("contextmenu", ".folder-driver", function(e) {

	var getClass = this.className.split('-');
	var last = getClass.pop();

	$(".menudriver-" + last).addClass("openmenu");

	if ($(".menudriver-" + last).prop("checked") == true) {
		$(".menudriver-" + last).prop("checked", false)
	} else {
		$(".menudriver-" + last).prop("checked", true)
	}

	return false;
});

$(".delete-folder").on('click', function(e) {

	e.preventDefault();

	var getClass = this.className.split(' ');
	var last = getClass.pop();

	var id = this.className.split(' ')[1]

	axios.get('/transporters/searchdriver/folder/delete/' + id + '/' + last, {
			params: {
				cpf: last
			}
		})
		.then(function(response) {
			location.reload();
		})
		.catch(function(error) {
		})

});


//PANEL DRIVER

 
$(".icon_notification").on("click", function(){
  $(".container_notifcations").css("display", "block");
});


$(".update-message-read").submit(function(e){
  e.preventDefault();

  if ($('.icon_notification span').html() > 0){

    jQuery.ajax({
      type: 'POST', 
      url: '/transporter/driver/verify-message', 
      success: function(response) {
      
      
      
      
      }
    }); 

  }
  $(".icon_notification span").hide();

});


localStorage.removeItem('numero');

$(".btn-close-modal").click(function(e){
  $(".result-api-idwall").hide();
  $(".result-api-idwall .content").hide();
});

$("#enviar-image").click(function(e){

  e.preventDefault();

  if($(".image-match").val() == '' || $(".image-doc").val() == ''){
    $(".result-api-idwall").hide();
    $(".check-inputs").fadeIn();
    return;
  }

  $(".check-inputs").hide();
  $(".result-api-idwall").css("display", "flex");
  $(".autenticate-check").css("display", "block");
  

  var face = new FormData($("form[name='autenticate']")[0]);
  var result = null;

  axios({
    method: "post",
    url: "/driver/autenticate",
    data: face,
    headers: { "Content-Type": "multipart/form-data" },
  })
  .then(function (response) {

    let timer = setTimeout(function(){ 
      $('.btn-send').click();
    }, 15000);

      tokenCode = response["data"];
    
    if (localStorage.getItem('numero') == null) {     
      token = localStorage.setItem('numero', tokenCode)
      
    } else {
      
    }
    result = localStorage.getItem('numero');

    
    axios.get('/driver/autenticate/result-relatory', {
      params: {
        token: result
      }
    })
    .then(function (response) {
      
      let result = response["data"]["result"]["resultado"];

      if(result == "INVALID"){
        clearTimeout(timer);

        $(".autenticate-check").hide();
        $(".autenticate-not-confirmed").css("display", "block");

        localStorage.removeItem('numero');

        $('.autenticate-post')[0].reset();

      } 
      
    if(result == "VALID") {
        clearTimeout(timer);

        $(".autenticate-check").hide();
        $(".autenticate-confirmed").css("display", "block");

        localStorage.removeItem('numero');

        $('.autenticate-post')[0].reset();

		axios.post('/driver/autenticate/confirmidentityuser')
		.then(function (response) {
		})

		axios.post('/driver/autenticate/confirmaccountuser')
		.then(function (response) {
		})
	}

    })
    .catch(function (error) {
    })
  })
 
  

});


$(".reset-localstorage").click(function(e){

  e.preventDefault();

  localStorage.removeItem('numero');

  $('.autenticate-post')[0].reset();

});



function mascara(o, f) {
  v_obj = o
  v_fun = f
  setTimeout("execmascara()", 1)
}

function execmascara() {
  v_obj.value = v_fun(v_obj.value)
}

function pispasep(v) {
    v = v.replace(/\D/g, "")                                      //Remove tudo o que não é dígito
    v = v.replace(/^(\d{3})(\d)/, "$1.$2")                        //Coloca ponto entre o terceiro e o quarto dígitos
    v = v.replace(/^(\d{3})\.(\d{5})(\d)/, "$1.$2.$3")            //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/(\d{3})\.(\d{5})\.(\d{2})(\d)/, "$1.$2.$3.$4") //Coloca ponto entre o décimo e o décimo primeiro dígitos
    return v
}

$(".msg-error").hide();
$(".msg-success").hide();

$(".form-pis button").click(function(e){

e.preventDefault();

validarPIS($(".input-pis-value").val())

function validarPIS(pis) {

  var multiplicadorBase = "3298765432";
  var total = 0;
  var resto = 0;
  var multiplicando = 0;
  var multiplicador = 0;
  var digito = 99;

  // Retira a mascara
  var numeroPIS = pis.replace(/[^\d]+/g, '');
  if (numeroPIS.length !== 11 ||
      numeroPIS === "00000000000" ||
      numeroPIS === "11111111111" ||
      numeroPIS === "22222222222" ||
      numeroPIS === "33333333333" ||
      numeroPIS === "44444444444" ||
      numeroPIS === "55555555555" ||
      numeroPIS === "66666666666" ||
      numeroPIS === "77777777777" ||
      numeroPIS === "88888888888" ||
      numeroPIS === "99999999999") {
      $(".msg-error").show();
      $(".msg-error").html("Número do PIS errado, verifique e tente novamente!");
      return false;
    
  } else {
    $(".msg-error").hide();
      for (var i = 0; i < 10; i++) {
          multiplicando = parseInt(numeroPIS.substring(i, i + 1));
          multiplicador = parseInt(multiplicadorBase.substring(i, i + 1));
          total += multiplicando * multiplicador;
      }
      resto = 11 - total % 11;
      resto = resto === 10 || resto === 11 ? 0 : resto;
      digito = parseInt("" + numeroPIS.charAt(10));
    

      axios.post('/driver/data/pis', {
        pis: numeroPIS,
      })
      .then(function (response) {
        
      })
      .catch(function (error) {
      });

      $(".msg-success").show();
      $(".msg-success").html("PIS cadastrado com sucesso!");
	  axios.post('/driver/autenticate/confirmaccountuser')
		.then(function (response) {
		})
      return true;    
  }
}
});

$(".ti-bell").on("click", function(){
	$(".ti-bell").css("color", "#345b95");
	$(".content-messages").toggleClass("openmessages");

});

$(".update-message-read-driver").click(function(e) {
	e.preventDefault();
	
	if ($('.notificate').html() > 0) {
		axios({
				method: "post",
				url: "/driver/verify-message",
			})
			.then(function(response) {})
	}
	$(".notificate").hide();
});


$('.card-number-input').mask('9999 9999 9999 9999')
$('.card-expiration-input').mask('99/99')
$('.card-expiration-input').mask('99/9999')
$('.card-security-input').mask('999')


$('.cnpj_transporter').mask('99.999.999/9999-99')


