<?php

require_once "../bootstrap.php";

$app->get('/transporter/login', 'app\controllers\transporter\LoginController:index');
$app->post('/transporter/login', 'app\controllers\transporter\LoginController:store');

$app->group('/transporter', function() use($app){
    $app->get('/painel',                        'app\controllers\transporter\PainelController:index');
    $app->get('/historic/searchdriver/{id}',    'app\controllers\transporter\PainelController:backpSearch');
    $app->get('/drivers',                       'app\controllers\transporter\PainelController:drivers');
    $app->get('/account',                       'app\controllers\transporter\PainelController:account');
    $app->post('/account',                      'app\controllers\transporter\PainelController:accountUpdate');
    // $app->post('/user/photo/update',            'app\controllers\PerfilPhotoController:store');
    $app->get('/logout',                 'app\controllers\transporter\PainelController:destroy');
})->add($middleware->transporter());

$app->run();